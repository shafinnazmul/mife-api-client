package bd.com.robi.devops.constant;

import org.apache.http.HttpStatus;

public class LicenseIssuerResponseConstant {

    public static final String HTTP_RESPONSE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public static final String RESPONSE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public static final String HTTP_RESPONSE_DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";


    //STATUS CODE : E0000
    public static final String VALID_RESPONSE_CODE = "00000";
    public static final Integer VALID_RESPONSE_STATUS = HttpStatus.SC_OK;
    public static final Integer VALID_MULTI_RESPONSE_STATUS = HttpStatus.SC_MULTI_STATUS;
    public static final String VALID_RESPONSE_MESSAGE = "OK";

    //STATUS CODE : E0001
    public static final String INVALID_PARAM_RESPONSE_CODE = "E0001";
    public static final Integer INVALID_PARAM_RESPONSE_STATUS = HttpStatus.SC_BAD_REQUEST;
    public static final String INVALID_PARAM_RESPONSE_MESSAGE = "Invalid parameter";
    public static final String INVALID_XML_RESPONSE_MESSAGE = "Invalid tag in requestBody";


    //STATUS CODE : E0002
    public static final String UNSUPPORTED_MEDIA_TYPE_RESPONSE_CODE = "E0002";
    public static final Integer UNSUPPORTED_MEDIA_TYPE_RESPONSE_STATUS = HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE;
    public static final String UNSUPPORTED_MEDIA_TYPE_RESPONSE_MESSAGE = "Unsupported media type";

    //STATUS CODE : E0003
    public static final String INVALID_AUTH_TOKEN_RESPONSE_CODE = "E0003";
    public static final Integer INVALID_AUTH_TOKEN_RESPONSE_STATUS = HttpStatus.SC_UNAUTHORIZED;
    public static final String INVALID_AUTH_TOKEN_RESPONSE_MESSAGE = "Invalid Authorization Key";

    //STATUS CODE : E0004
    public static final String INVALID_AUTH_HEADER_RESPONSE_CODE = "E0004";
    public static final Integer INVALID_AUTH_HEADER_RESPONSE_STATUS = HttpStatus.SC_UNAUTHORIZED;
    public static final String INVALID_AUTH_HEADER_RESPONSE_MESSAGE = "Invalid Authorization Header";

    //STATUS CODE : E0005
    public static final String INTERNAL_SERVER_ERROR_RESPONSE_CODE = "E0005";
    public static final Integer INTERNAL_SERVER_ERROR_RESPONSE_STATUS = HttpStatus.SC_INTERNAL_SERVER_ERROR;
    public static final String INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE = "Internal server error";

    //STATUS CODE : E0006
    public static final String SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE = "E0006";
    public static final Integer SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS = HttpStatus.SC_INTERNAL_SERVER_ERROR;
    public static final String SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE = "Error from inner API";


    //STATUS CODE : E0007
    public static final String DB_CONNECTION_ERROR_RESPONSE_CODE = "E0007";
    public static final Integer DB_CONNECTION_ERROR_RESPONSE_STATUS = HttpStatus.SC_INTERNAL_SERVER_ERROR;
    public static final String DB_CONNECTION_ERROR_RESPONSE_MESSAGE = "Can not connect to DB";


    //STATUS CODE : E0008
    public static final String INVALID_REQUEST_METHOD_RESPONSE_CODE = "E0008";
    public static final Integer INVALID_REQUEST_METHOD_RESPONSE_STATUS = HttpStatus.SC_METHOD_NOT_ALLOWED;
    public static final String INVALID_REQUEST_METHOD_RESPONSE_MESSAGE = "Method not allowed";

    //STATUS CODE : E0010
    public static final String UNEXPECTED_DB_DATA_RESPONSE_CODE = "E0010";
    public static final Integer UNEXPECTED_DB_DATA_RESPONSE_STATUS = HttpStatus.SC_BAD_REQUEST;
    public static final String UNEXPECTED_DB_DATA_RESPONSE_MESSAGE = "Invalid data from DB";

    //STATUS CODE : E0011
    public static final String UNEXPECTED_API_DATA_RESPONSE_CODE = "E0011";
    public static final Integer UNEXPECTED_API_DATA_RESPONSE_STATUS = HttpStatus.SC_BAD_REQUEST;
    public static final String UNEXPECTED_API_DATA_RESPONSE_MESSAGE = "Invalid data from API";
    public static final String ERROR_IN_DONKEY_POSITIONS_API_RESPONSE_MESSAGE = "positions not found in donkey API";
    public static final String ERROR_IN_MODERATOR_VENDOR_API_POST_MESSAGE = "agent not created in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_DELEGATE_API_GET_MESSAGE = "delegate not found in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_SERVICE_API_GET_MESSAGE = "service not found in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_SERVICE_API_POST_MESSAGE = "service not created in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_SERVICE_API_PUT_MESSAGE = "service not updated in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_LICENSE_TEMPLATE_API_GET_MESSAGE = "licenseTemplate not found in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_LICENSE_API_POST_MESSAGE = "license not created in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_LICENSE_API_GET_MESSAGE = "license not found in moderatorAPI";
    public static final String ERROR_IN_DONKEY_GROUP_API_MESSAGE = "group not found in donkeyAPI";
    public static final String ERROR_IN_WINES_SERVICE_API_MESSAGE = "serviceName not found in winesAPI";
    public static final String ERROR_IN_MODERATOR_LICENSE_API_PUT_MESSAGE = "license can not be updated in moderatorAPI";

    public static final String ERROR_IN_MODERATOR_LICENSE_DELEGATE_API_GET_MESSAGE = "license delegate not found in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_LICENSE_DELEGATE_API_PUT_MESSAGE = "license delegate not updated in moderatorAPI";

    public static final String ERROR_IN_MODERATOR_DELEGATE_AGENT_COND_API_POST_MESSAGE = "delegate agent cond not created in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_DELEGATE_AGENT_COND_API_PUT_MESSAGE = "delegate agent cond not updated in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_DELEGATE_AGENT_COND_API_GET_MESSAGE = "delegate agent cond not found in moderatorAPI";
    public static final String ERROR_IN_MODERATOR_DELEGATE_AGENT_COND_API_DELETE_MESSAGE = "delegate agent cond not deleted in moderatorAPI";

    public static final String ERROR_IN_MODERATOR_LICENSES_API_GET_MESSAGE = "licenses not found in moderatorAPI";

    //STATUS CODE : E0012
    public static final String INVALID_AUTH_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE = "E0012";
    public static final Integer INVALID_AUTH_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS = HttpStatus.SC_INTERNAL_SERVER_ERROR;
    public static final String INVALID_AUTH_ERROR_FROM_EXTERNAL_API_MESSAGE = "Sending Invalid key to inner";
}
