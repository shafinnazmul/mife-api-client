package bd.com.robi.devops.support;

import bd.com.robi.devops.constant.LicenseIssuerResponseConstant;
import bd.com.robi.devops.exception.ApiException;
import bd.com.robi.devops.exception.SystemException;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.WebApplicationException;


@Slf4j
public class ThrowableException {
    public void throwException(WebApplicationException e, String requestId) throws ApiException {
        Integer status = e.getResponse().getStatus();
        if (status == 401) {
            log.error("requestId:" + requestId + ", Internal server error from external api", e);
            throw new SystemException(LicenseIssuerResponseConstant.INVALID_AUTH_ERROR_FROM_EXTERNAL_API_MESSAGE,
                    LicenseIssuerResponseConstant.INVALID_AUTH_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE, LicenseIssuerResponseConstant.INVALID_AUTH_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        }
        if (status == 404) {
            log.error("requestId:" + requestId + ", Internal server error from external api", e);
            throw new SystemException(LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE, LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        }
        if (status >= 500 && status <= 599) {
            log.error("requestId:" + requestId + ", Internal server error from external api", e);
            throw new SystemException(LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE, LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        }
    }
}
