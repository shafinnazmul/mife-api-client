package bd.com.robi.devops.exception;


import bd.com.robi.devops.constant.LicenseIssuerResponseConstant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class WrongParameterException extends ApiException {
    private static final long serialVersionUID = 3464323083840792685L;
    public static WrongParameterException DEFAULT_WRONG_PARAMETER_EXCEPTION = new WrongParameterException(
            LicenseIssuerResponseConstant.INVALID_PARAM_RESPONSE_MESSAGE,
            LicenseIssuerResponseConstant.INVALID_PARAM_RESPONSE_CODE,
            LicenseIssuerResponseConstant.INVALID_PARAM_RESPONSE_STATUS
            );
    private int httpStatusCode = LicenseIssuerResponseConstant.INVALID_PARAM_RESPONSE_STATUS;
    private String statusCode = LicenseIssuerResponseConstant.INVALID_PARAM_RESPONSE_CODE;
    private String message = LicenseIssuerResponseConstant.INVALID_PARAM_RESPONSE_MESSAGE;
    private String errorCode;

    public WrongParameterException(String message) {
        super(message);
    }

    public WrongParameterException(String message, String errCode) {
        super(message);
        this.setErrorCode(errCode);
    }

    public WrongParameterException(Throwable cause) {
        super(cause);
    }

    public WrongParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongParameterException(String message, Throwable cause, boolean enableSuppression,
                                   boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public WrongParameterException(String message, String statusCode, int httpStatusCode) {
        super(message);
        setMessage(message);
        setStatusCode(statusCode);
        setHttpStatusCode(httpStatusCode);
    }
}
