package bd.com.robi.devops.exception;

public class XmlToHashmapConversionException extends Exception{

	private static final long serialVersionUID = 1997753363232807012L;

	public XmlToHashmapConversionException() {
	}

	public XmlToHashmapConversionException(String message) {
		super(message);
	}

	public XmlToHashmapConversionException(Throwable cause) {
		super(cause);
	}

	public XmlToHashmapConversionException(String message, Throwable cause) {
		super(message, cause);
	}

	public XmlToHashmapConversionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
			{
				super(message, cause, enableSuppression, writableStackTrace);
			}

}