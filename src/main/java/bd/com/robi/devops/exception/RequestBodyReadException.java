package bd.com.robi.devops.exception;

public class RequestBodyReadException extends Exception{

	private static final long serialVersionUID = 1997753363232807009L;

	public RequestBodyReadException() {
	}

	public RequestBodyReadException(String message) {
		super(message);
	}

	public RequestBodyReadException(Throwable cause) {
		super(cause);
	}

	public RequestBodyReadException(String message, Throwable cause) {
		super(message, cause);
	}

	public RequestBodyReadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
			{
				super(message, cause, enableSuppression, writableStackTrace);
			}

}