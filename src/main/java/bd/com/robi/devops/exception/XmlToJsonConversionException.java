package bd.com.robi.devops.exception;

public class XmlToJsonConversionException extends Exception{

	private static final long serialVersionUID = 1997753363232807012L;

	public XmlToJsonConversionException() {
	}

	public XmlToJsonConversionException(String message) {
		super(message);
	}

	public XmlToJsonConversionException(Throwable cause) {
		super(cause);
	}

	public XmlToJsonConversionException(String message, Throwable cause) {
		super(message, cause);
	}

	public XmlToJsonConversionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
			{
				super(message, cause, enableSuppression, writableStackTrace);
			}

}