package bd.com.robi.devops.exception;

import bd.com.robi.devops.constant.LicenseIssuerResponseConstant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SystemException extends ApiException {
    private int httpStatusCode = LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS;
    private String statusCode = LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE;
    private String message = LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE;

    public static final SystemException INTERNAL_SERVER_EXCEPTION = new SystemException(
            LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
            LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
            LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);

    private static final long serialVersionUID = 5233568973791209828L;
    private String errorCode;

    public SystemException(String message) {
        super(message);
    }

    public SystemException(String message, String errCode) {
        super(message);
        this.setErrorCode(errCode);
    }

    public SystemException(Throwable cause) {
        super(cause);
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public SystemException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public SystemException(String message, String statusCode, int httpStatusCode) {
        super(message);
        setMessage(message);
        setStatusCode(statusCode);
        setHttpStatusCode(httpStatusCode);
    }
}
