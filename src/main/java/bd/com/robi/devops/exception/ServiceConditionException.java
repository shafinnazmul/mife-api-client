package bd.com.robi.devops.exception;

import bd.com.robi.devops.constant.LicenseIssuerResponseConstant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ServiceConditionException extends ApiException {
    private int httpStatusCode = LicenseIssuerResponseConstant.UNEXPECTED_API_DATA_RESPONSE_STATUS;
    private String statusCode = LicenseIssuerResponseConstant.UNEXPECTED_API_DATA_RESPONSE_CODE;
    private String message = LicenseIssuerResponseConstant.UNEXPECTED_API_DATA_RESPONSE_MESSAGE;

    private static final long serialVersionUID = -3745397308132147157L;

    public ServiceConditionException(String message) {
        super(message);
    }

    public ServiceConditionException(Throwable cause) {
        super(cause);
    }

    public ServiceConditionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceConditionException(String message, Throwable cause, boolean enableSuppression,
                                     boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ServiceConditionException(String message, String statusCode, int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
        this.statusCode = statusCode;
        this.message = message;
    }

    public static final ServiceConditionException DEFAULT_UNEXPECTED_API_DATA_EXCEPTION = new ServiceConditionException
            (LicenseIssuerResponseConstant.UNEXPECTED_API_DATA_RESPONSE_MESSAGE,
            LicenseIssuerResponseConstant.UNEXPECTED_API_DATA_RESPONSE_CODE,
            LicenseIssuerResponseConstant.UNEXPECTED_API_DATA_RESPONSE_STATUS);
}
