package bd.com.robi.devops.exception;

public class NoRequestBodyFoundException extends Exception{

	private static final long serialVersionUID = 1997753363232807010L;

	public NoRequestBodyFoundException() {
	}

	public NoRequestBodyFoundException(String message) {
		super(message);
	}

	public NoRequestBodyFoundException(Throwable cause) {
		super(cause);
	}

	public NoRequestBodyFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoRequestBodyFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
			{
				super(message, cause, enableSuppression, writableStackTrace);
			}

}