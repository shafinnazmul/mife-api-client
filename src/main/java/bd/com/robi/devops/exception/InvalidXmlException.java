package bd.com.robi.devops.exception;

public class InvalidXmlException extends Exception{

	private static final long serialVersionUID = 1997753363232807009L;

	public InvalidXmlException() {
	}

	public InvalidXmlException(String message) {
		super(message);
	}

	public InvalidXmlException(Throwable cause) {
		super(cause);
	}

	public InvalidXmlException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidXmlException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
			{
				super(message, cause, enableSuppression, writableStackTrace);
			}

}