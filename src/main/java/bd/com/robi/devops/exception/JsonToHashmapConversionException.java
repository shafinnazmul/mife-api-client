package bd.com.robi.devops.exception;

public class JsonToHashmapConversionException extends Exception{

	private static final long serialVersionUID = 1997753363232807012L;

	public JsonToHashmapConversionException() {
	}

	public JsonToHashmapConversionException(String message) {
		super(message);
	}

	public JsonToHashmapConversionException(Throwable cause) {
		super(cause);
	}

	public JsonToHashmapConversionException(String message, Throwable cause) {
		super(message, cause);
	}

	public JsonToHashmapConversionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
			{
				super(message, cause, enableSuppression, writableStackTrace);
			}

}