package com.mashfin.licenseissuer.mife.api.endpoint.dto.provider;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "providerResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class ProviderResponse implements Serializable {

    private static final long serialVersionUID = -8048325691925078246L;
    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "provider")
    private Provider provider;


    public static ProviderResponse empty() {
        return ProviderResponse.builder()
                .provider(null)
                .responseStatus(null)
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(getProvider());
    }
}
