package com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.List;


@Builder(builderClassName = "Builder", toBuilder = true)
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DelegateIdsRequest implements Serializable {
    private static final long serialVersionUID = -5068449862758196974L;

    private List<String> delegateIds;

}
