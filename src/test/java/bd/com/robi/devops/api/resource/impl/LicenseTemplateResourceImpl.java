package com.mashfin.licenseissuer.mife.api.resource.impl;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.LicenseTemplateParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.LicenseTemplatePost;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.LicenseTemplateResponse;
import com.mashfin.licenseissuer.mife.api.resource.LicenseTemplateResource;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import com.mashfin.licenseissuer.mife.api.support.JAXRSClientUtil;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.*;


@Slf4j
public class LicenseTemplateResourceImpl implements LicenseTemplateResource {
    private static final String LICENSE_TEMPLATES_GET_URI = "/moderator/operator/licensetemplate";

    private ApiConnectionProvider authProvider;

    public LicenseTemplateResourceImpl(ApiConnectionProvider apiConnectionProvider) {
        this.authProvider = apiConnectionProvider;
    }

    public String serviceEndPoint(String url) {
        return authProvider.serviceUri() + url;
    }

    @Override
    public LicenseTemplateParentResponse addLicenseTemplate(LicenseTemplatePost licenseTemplatePost) {
        try {
            checkRequiredParams(licenseTemplatePost);
            try {
                LicenseTemplateResourceMapping resourceMapping = JAXRSClientUtil.prepareResource(
                        serviceEndPoint(LICENSE_TEMPLATES_GET_URI),
                        LicenseTemplateResourceMapping.class,
                        authProvider);


                LicenseTemplateResponse response = resourceMapping.addLicenseTemplate(licenseTemplatePost);

                return LicenseTemplateParentResponse.builder()
                        .licenseTemplateResponse(response)
                        .httpResponseStatus(200)
                        .build();
            } catch (NotAuthorizedException | BadRequestException ex) {
                return LicenseTemplateParentResponse.builder()
                        .errorResponse(ex.getResponse().readEntity(ErrorResponse.class))
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();
            } catch (InternalServerErrorException | NotAllowedException | NotSupportedException | NotFoundException ex) {
                return LicenseTemplateParentResponse.builder()
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();
            } catch (NullPointerException ex) {
                return LicenseTemplateParentResponse.builder().build();
            }
        } catch (Exception e) {
            log.error("opps! what's happened ", e);
        }
        return null;
    }

    @Override
    public LicenseTemplateParentResponse getLicenseTemplate(String licenseTemplateId) {
//        Preconditions.checkRequired(licenseTemplateId, "licenseTemplateId is required.");
        try {
            try {
                LicenseTemplateResourceMapping resourceMapping = JAXRSClientUtil.prepareResource(
                        serviceEndPoint(LICENSE_TEMPLATES_GET_URI), LicenseTemplateResourceMapping.class, authProvider);

                LicenseTemplateResponse response = resourceMapping.getLicenseTemplate(licenseTemplateId);

                return LicenseTemplateParentResponse.builder()
                        .licenseTemplateResponse(response)
                        .httpResponseStatus(200)
                        .build();
            } catch (NotAuthorizedException | BadRequestException ex) {
                return LicenseTemplateParentResponse.builder()
                        .errorResponse(ex.getResponse().readEntity(ErrorResponse.class))
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();
            } catch (InternalServerErrorException | NotAllowedException | NotSupportedException | NotFoundException ex) {
                return LicenseTemplateParentResponse.builder()
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();
            } catch (NullPointerException ex) {
                return LicenseTemplateParentResponse.builder().build();
            }
        } catch (Exception e) {
            log.error("opps! what's happened ", e);
        }
        return null;
    }

    private void checkRequiredParams(LicenseTemplatePost licenseTemplatePost) {
//        Preconditions.checkRequired(licenseTemplatePost, "License Templates params are required.");
//        Preconditions.checkRequired(licenseTemplatePost.getServiceRequest(), "ServiceRequest param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getLicenseTemplateRequest(),                "LicenseTemplateRequest param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getServiceRequest().getServiceId(),                "ServiceId param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getLicenseTemplateRequest().getLicensetemplateName(), "LicensetemplateName param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getLicenseTemplateRequest().                getRegistableState(), "RegistableState param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getLicenseTemplateRequest().                getChargeableState(), "ChargeableState param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getLicenseTemplateRequest().                getReachableState(), "ReachableState param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getLicenseTemplateRequest().                getDemoVisible(), "DemoVisible param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getLicenseTemplateRequest().                getMaxQueryPercentage(), "MaxQueryPercentage param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getLicenseTemplateRequest().                getModeratorApprovalState(), "ModeratorApprovalState param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getLicenseTemplateRequest().                getVendorApprovalState(), "VendorApprovalState param is required.");
//        Preconditions.checkRequired(licenseTemplatePost.getLicenseTemplateRequest().                getProviderApprovalState(), "ProviderApprovalState param is required.");
    }

    public interface LicenseTemplateResourceMapping {
        @POST
        LicenseTemplateResponse addLicenseTemplate(LicenseTemplatePost licenseTemplatePost)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @GET
        LicenseTemplateResponse getLicenseTemplate(@QueryParam(value = "licensetemplateId") String licenseTemplateId)
                throws ProcessingException, NotAuthorizedException, NullPointerException;
    }
}
