package com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "delegatesResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicenseAuthDelegatesResponse implements Serializable {

    private static final long serialVersionUID = 8727232112995640174L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "totalCount")
    private Integer totalCount;

    @XmlElement(name = "delegates")
    private LicenseAuthDelegates licenseAuthDelegates;


    public static LicenseAuthDelegatesResponse empty() {
        return LicenseAuthDelegatesResponse.builder()
                .licenseAuthDelegates(null)
                .responseStatus(null)
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(getLicenseAuthDelegates());
    }
}
