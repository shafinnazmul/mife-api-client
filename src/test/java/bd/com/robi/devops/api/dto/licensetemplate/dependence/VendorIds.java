package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.dependence;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class VendorIds implements Serializable {

    private static final long serialVersionUID = -1909855244951682389L;

    private List<String> vendorId;

}
