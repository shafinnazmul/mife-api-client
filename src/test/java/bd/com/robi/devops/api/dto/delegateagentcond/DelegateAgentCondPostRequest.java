package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@XmlRootElement(name = "delegateagentcondCreateRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "Builder", toBuilder = true)
public class DelegateAgentCondPostRequest implements Serializable {
    private static final long serialVersionUID = 7125852233505403161L;

    @XmlElement(name = "delegateagentcondRequest")
    private DelegateAgentCondRequest delegateAgentCondRequest;
}
