package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Builder;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;


@Builder
@RequiredArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessRequestApprovalInfo implements Serializable {
    private static final long serialVersionUID = -2604344154933071260L;

    @NotNull
    private String requestId;

    @NotNull
    private String approverAccount;

    @NotNull
    private Integer status;

    @NotNull
    private String requesterEmail;

    private String note;

    @NotNull
    private Integer action;

    @AllArgsConstructor
    @RequiredArgsConstructor
    @Getter
    @Setter
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AccessRequestApprovalWrapper implements Serializable {

        private static final long serialVersionUID = -8229994710038111287L;

        @XmlElement(name = "accessRequest")
        private List<AccessRequestApprovalInfo> items;
    }

}
