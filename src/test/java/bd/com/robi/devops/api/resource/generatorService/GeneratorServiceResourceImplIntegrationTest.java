package bd.com.robi.devops.api.resource.generatorService;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService.*;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.service.VendorRequest;
import com.mashfin.licenseissuer.mife.api.resource.GeneratorServiceResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.UUID;

import static java.util.Arrays.asList;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {GeneratorServiceContext.class})
public class GeneratorServiceResourceImplIntegrationTest {
    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private GeneratorServiceResource generatorServiceResource;

    @Test
    public void postGeneratorServiceTest() {
        String id = UUID.randomUUID().toString();
        GeneratorServiceParentResponse generatorServicePostParentResponse = generatorServiceResource.postGeneratorService(
                GeneratorServiceRequestBody.builder()
                        .delegateIdsRequest(DelegateIdsRequest.builder()
                                .delegateIds(null)
                                .build())
                        .generatorLicenseTemplateRequest(GeneratorLicenseTemplateRequest.builder()
                                .licensetemplateName("_" + id)
                                .aliasName("test")
                                .contactMail("marie1@gmail.com")
                                .description("Project for service test")
                                .descriptionUrl("sample desc url")
                                .downloadUrl("sample download url")
                                .limitDay(30)
                                .build())
                        .generatorServiceRequest(GeneratorServiceRequest.builder()
                                .serviceName(id)
                                .aliasName("test")
                                .contactMail("marie@gmail.com")
                                .description("Project for service test")
                                .descriptionUrl("project for saving server")
                                .prefix("LI")
                                .build())
                        .vendorRequest(VendorRequest.builder()
                                .vendorId("aea47e40-2d8b-11e8-af75-425732242c5c")
                                .build())
                        .build());

        Assert.notNull(generatorServicePostParentResponse.getGeneratorServiceResponse());
    }

    @Test
    public void putGeneratorServiceTest() throws Exception {
        String[] delegateId = {"0051dd10-e221-11e3-9790-0019b9e60f26", "00f50260-9c93-11e4-a938-0019b9e60f26"};
        GeneratorServiceParentResponse generatorServicePostParentResponse = generatorServiceResource.putGeneratorService(
                GeneratorServiceRequestBody.builder()
                        .delegateIdsRequest(DelegateIdsRequest.builder()
                                .delegateIds(asList(delegateId))
                                .build())
                        .generatorLicenseTemplateRequest(GeneratorLicenseTemplateRequest.builder()
                                .licensetemplateId("ca7382c0-3ec4-11e8-af75-425732242c5c")
                                .licensetemplateName("_" + "09d46572-fb57-4155-a056-4bf73e281724")
                                .aliasName("test")
                                .contactMail("marie1@gmail.com")
                                .description("Project for service test")
                                .descriptionUrl("sample desc url")
                                .downloadUrl("sample download url")
                                .limitDay(30)
                                .build())
                        .generatorServiceRequest(GeneratorServiceRequest.builder()
                                .serviceId("ca730d90-3ec4-11e8-af75-425732242c5c")
                                .serviceName("09d46572-fb57-4155-a056-4bf73e281724")
                                .aliasName("test")
                                .contactMail("marie@gmail.com")
                                .description("Project for service test")
                                .descriptionUrl("project for saving server")
                                .build())
                        .vendorRequest(VendorRequest.builder()
                                .vendorId("aea47e40-2d8b-11e8-af75-425732242c5c")
                                .build())
                        .build());

        Assert.notNull(generatorServicePostParentResponse.getGeneratorServiceResponse());
    }
}
