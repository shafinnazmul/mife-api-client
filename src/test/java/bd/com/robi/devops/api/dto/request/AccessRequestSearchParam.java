package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.Builder;
import lombok.Getter;

import javax.ws.rs.QueryParam;


@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
public class AccessRequestSearchParam {

    private String requestId;

    private String requestDateFrom;

    private String requestDateTo;

    private String delegateGroupId;

    private String delegateId;

    private String vendorAliasName;

    private String requesterAccount;

    private Integer status;

    private String authorizationKey;

    private String serviceId;

    private String serviceName;

    private String providerId;

    @QueryParam("requestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @QueryParam("requestDateFrom")
    public void setRequestDateFrom(String requestDateFrom) {
        this.requestDateFrom = requestDateFrom;
    }

    @QueryParam("requestDateTo")
    public void setRequestDateTo(String requestDateTo) {
        this.requestDateTo = requestDateTo;
    }

    @QueryParam("delegateGroupId")
    public void setDelegateGroupId(String delegateGroupId) {
        this.delegateGroupId = delegateGroupId;
    }

    @QueryParam("delegateId")
    public void setDelegateId(String delegateId) {
        this.delegateId = delegateId;
    }

    @QueryParam("vendorAliasName")
    public void setVendorAliasName(String vendorAliasName) {
        this.vendorAliasName = vendorAliasName;
    }

    @QueryParam("requesterAccount")
    public void setRequesterAccount(String requesterAccount) {
        this.requesterAccount = requesterAccount;
    }

    @QueryParam("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @QueryParam("authorizationKey")
    public void setAuthorizationKey(String authorizationKey) {
        this.authorizationKey = authorizationKey;
    }

    @QueryParam("serviceId")
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    @QueryParam("serviceName")
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @QueryParam("providerId")
    public void setProviderAliasName(String providerId) {
        this.providerId = providerId;
    }
}
