package com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class ServiceIds implements Serializable {

    private static final long serialVersionUID = -1556001571734674230L;

    @XmlElement(name = "serviceId")
    private List<String> serviceIds;
}
