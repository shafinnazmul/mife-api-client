package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "delegateResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class DelegateResponse implements Serializable {

    private static final long serialVersionUID = -200785515528734646L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "delegate")
    private Delegate delegate;


    public static DelegateResponse empty() {
        return DelegateResponse.builder()
                .delegate(null)
                .responseStatus(null)
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(getDelegate());
    }
}
