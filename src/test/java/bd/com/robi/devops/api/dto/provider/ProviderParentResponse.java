package com.mashfin.licenseissuer.mife.api.endpoint.dto.provider;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import lombok.*;

import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProviderParentResponse implements Serializable {

    private static final long serialVersionUID = -6893413956350532165L;

    private ErrorResponse errorResponse;

    private ProviderResponse providerResponse;


    public static ProviderParentResponse empty() {
        return ProviderParentResponse.builder()
                .providerResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasProviderResponse() {
        return Objects.nonNull(getProviderResponse());
    }
}
