package com.mashfin.licenseissuer.mife.api.resource;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates.LicenseTemplatesGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates.LicenseTemplatesParentResponse;


public interface LicenseTemplatesResource {
    LicenseTemplatesParentResponse getLicenseTemplates(LicenseTemplatesGetParams licenseTemplatesGetParams);
}
