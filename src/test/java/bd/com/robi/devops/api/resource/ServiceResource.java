package com.mashfin.licenseissuer.mife.api.resource;

import bd.com.robi.devops.exception.ApiException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.service.*;


public interface ServiceResource {
    ServiceGetParentResponse getService(ServiceGetParams serviceGetParams, String requestId) throws ApiException;

    ServicePostParentResponse postService(ServicePostBody servicePostBody);

    ServicePutParentResponse putService(ServicePutBody servicePutBody, String requestId) throws ApiException;
}
