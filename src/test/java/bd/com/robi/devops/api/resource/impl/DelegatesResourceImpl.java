package com.mashfin.licenseissuer.mife.api.resource.impl;


import bd.com.robi.devops.constant.LicenseIssuerResponseConstant;
import bd.com.robi.devops.exception.ApiException;
import bd.com.robi.devops.exception.SystemException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegates.DelegatesGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegates.DelegatesParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegates.DelegatesResponse;
import com.mashfin.licenseissuer.mife.api.resource.DelegatesResource;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import com.mashfin.licenseissuer.mife.api.support.JAXRSClientUtil;
import bd.com.robi.devops.support.ThrowableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Slf4j
public class DelegatesResourceImpl implements DelegatesResource {


    @Bean
    private ThrowableException throwable() {
        return new ThrowableException();
    }

    private ApiConnectionProvider authProvider;

    public DelegatesResourceImpl(ApiConnectionProvider authProvider) {
        this.authProvider = authProvider;
    }

    private String serviceEndPoint(String providerUri) {
        return authProvider.serviceUri() + providerUri;
    }

    private String getURI() {
        return serviceEndPoint(("mife.operator.delegates.action"));
    }

    @Override
    public DelegatesParentResponse getDelegates(DelegatesGetParams delegatesGetParams, String requestId) throws ApiException {
        try {
            DelegatesResponse delegatesResponse = JAXRSClientUtil.prepareResource(getURI(),
                    DelegatesResourceMapping.class, authProvider).getDelegates(delegatesGetParams);

            return DelegatesParentResponse.builder()
                    .delegatesResponse(delegatesResponse)
                    .httpResponseStatus(200)
                    .build();
        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return DelegatesParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }

    public interface DelegatesResourceMapping {
        @GET
        DelegatesResponse getDelegates(@BeanParam DelegatesGetParams params)
                throws ProcessingException, NotAuthorizedException, NullPointerException;
    }
}
