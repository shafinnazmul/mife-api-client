package com.mashfin.licenseissuer.mife.api.resource;

import bd.com.robi.devops.exception.ApiException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate.DelegateGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate.DelegateParentResponse;


public interface DelegateResource {
    DelegateParentResponse getDelegate(DelegateGetParams delegateGetParams, String requestId) throws ApiException;
}
