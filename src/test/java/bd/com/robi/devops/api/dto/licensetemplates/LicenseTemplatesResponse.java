package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates.dependence.Dependence;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "licensetemplatesResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicenseTemplatesResponse implements Serializable {

    private static final long serialVersionUID = 3048949626183760372L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "totalCount")
    private Integer totalCount;

    @XmlElement(name = "licensetemplates")
    private LicenseTemplate.LicenseTemplateWrapper licenseTemplates;

    @XmlElement(name = "dependences")
    private Dependence.DependenceWrapper dependences;

    public static LicenseTemplatesResponse empty() {
        return LicenseTemplatesResponse.builder()
                .responseStatus(null)
                .totalCount(null)
                .dependences(null)
                .licenseTemplates(null)
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(this.getLicenseTemplates());
    }
}
