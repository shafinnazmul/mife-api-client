package com.mashfin.licenseissuer.mife.api.endpoint.dto.service;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Getter
@Setter
@XmlRootElement(name = "serviceRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "Builder", toBuilder = true)
public class ServiceRequest implements Serializable {

    private static final long serialVersionUID = -4460633779485859393L;

    private String serviceName;
    private String aliasName;
    private Integer registableState;
    private Integer reachableState;
    private Integer chargeableState;
    private Integer demoVisible;
    private String sourceIpaddress;
    private Integer maxQueryPercentage;
    private String contactMail;
    private String description;
    private String descriptionUrl;
    private Integer moderatorApprovalState;
    private Integer vendorApprovalState;
    private Integer providerApprovalState;
    private String prefix;

    public ServiceRequest(){}

    public ServiceRequest(String serviceName,
                          String aliasName,
                          Integer registableState,
                          Integer reachableState,
                          Integer chargeableState,
                          Integer demoVisible,
                          String sourceIpaddress,
                          Integer maxQueryPercentage,
                          String contactMail,
                          String description,
                          String descriptionUrl,
                          Integer moderatorApprovalState,
                          Integer vendorApprovalState,
                          Integer providerApprovalState,
                          String prefix) {
        this.serviceName = serviceName;
        this.aliasName = aliasName;
        this.registableState = registableState;
        this.reachableState = reachableState;
        this.chargeableState = chargeableState;
        this.demoVisible = demoVisible;
        this.sourceIpaddress = sourceIpaddress;
        this.maxQueryPercentage = maxQueryPercentage;
        this.contactMail = contactMail;
        this.description = description;
        this.descriptionUrl = descriptionUrl;
        this.moderatorApprovalState = moderatorApprovalState;
        this.vendorApprovalState = vendorApprovalState;
        this.providerApprovalState = providerApprovalState;
        this.prefix = prefix;
    }
}
