package com.mashfin.licenseissuer.mife.api.entity;

import lombok.Getter;

import java.util.stream.Stream;



@Getter
public enum SortOrder {
    ASCENDING("ascending"), DESCENDING("descending");

    private String value;

    SortOrder(String value) {
        this.value = value;
    }

    public static SortOrder findByValue(String value) {
        return Stream.of(SortOrder.values())
                .filter(a -> a.getValue().equals(value))
                .findFirst()
                .orElse(null);
    }

}
