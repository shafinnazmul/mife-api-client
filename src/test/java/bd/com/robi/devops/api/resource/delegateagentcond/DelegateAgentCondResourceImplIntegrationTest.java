package bd.com.robi.devops.api.resource.delegateagentcond;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond.DelegateAgentCondGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond.DelegateAgentCondParentResponse;
import com.mashfin.licenseissuer.mife.api.resource.DelegateAgentCondResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DelegateAgentCondContext.class})
public class DelegateAgentCondResourceImplIntegrationTest {

    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private DelegateAgentCondResource delegateAgentCondResource;

    @Test
    public void getDelegateErrorResponseTest() throws Exception {
        DelegateAgentCondParentResponse delegateAgentCondParentResponse = delegateAgentCondResource.getDelegateAgentCond(
                new DelegateAgentCondGetParams("04", "04"), "requestId");
        Assert.notNull(delegateAgentCondParentResponse.getErrorResponse());
    }

    @Test
    public void getDelegateAgentCondValidResponseTest() throws Exception {
        DelegateAgentCondParentResponse delegateAgentCondParentResponse = delegateAgentCondResource.getDelegateAgentCond(
                new DelegateAgentCondGetParams(
                        "1a485960-a41f-11e7-bc9a-0022196ab061", "13d04d30-a477-11e5-b703-0019b9e60f26"),
                "requestId");
        Assert.notNull(delegateAgentCondParentResponse.getDelegateAgentCondResponse().getDelegateAgentCond());
    }


}
