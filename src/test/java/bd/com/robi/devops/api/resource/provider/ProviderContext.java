package bd.com.robi.devops.api.resource.provider;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.ProviderResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.ProviderResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"bd.com.robi.devops.api.resource"})
public class ProviderContext extends BaseContext {

    @Bean
    public ProviderResource providerRepository() {
        return new ProviderResourceImpl(apiConnectionProvider(RequestorType.PROVIDER));
    }

}
