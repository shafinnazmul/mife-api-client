package com.mashfin.licenseissuer.mife.api.resource;

import bd.com.robi.devops.exception.ApiException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.VendorCreateRequest;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.VendorGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.VendorParentResponse;


public interface VendorResource {
    VendorParentResponse getVendor(VendorGetParams vendorGetParams, String requestId) throws ApiException;

    VendorParentResponse postVendor(VendorCreateRequest vendorCreateRequest, String requestId) throws ApiException;
}
