package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond;

import com.mashfin.licenseissuer.mife.api.entity.DateAdapter;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class DelegateAgentCond implements Serializable {

    private static final long serialVersionUID = -1540564371544499285L;

    private String delegateagentcondId;

    private String delegateId;

    private String agentId;


    private Integer maxQps;

    private Integer maxQpm;

    private Integer maxConnection;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date createTime;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date updateTime;

}
