package com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor;

import lombok.Builder;
import lombok.Getter;

import javax.ws.rs.QueryParam;
import java.io.Serializable;


@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
public class VendorGetParams implements Serializable {

    private static final long serialVersionUID = 115767465077866401L;

    private String vendorId;

    private String vendorName;

    public VendorGetParams(String vendorId, String vendorName) {
        this.vendorId = vendorId;
        this.vendorName = vendorName;
    }

    @QueryParam("vendorName")
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    @QueryParam("vendorId")
    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }
}
