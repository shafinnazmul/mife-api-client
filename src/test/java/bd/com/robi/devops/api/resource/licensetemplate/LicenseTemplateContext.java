package bd.com.robi.devops.api.resource.licensetemplate;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.LicenseTemplateResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.LicenseTemplateResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"com.mashfin.licenseissuer.mife.api.resource"})
public class LicenseTemplateContext extends BaseContext {
    @Bean
    public LicenseTemplateResource licenseTemplateResource() {
        return new LicenseTemplateResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }
}
