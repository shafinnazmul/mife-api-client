package com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.Returnable;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import lombok.*;

import java.io.Serializable;
import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VendorParentResponse implements Serializable, Returnable<Vendor, ResponseStatus, Dependence> {

    private static final long serialVersionUID = 7307524060326739592L;

    private ErrorResponse errorResponse;

    private VendorResponse vendorResponse;

    private Integer httpResponseStatus;

    public static VendorParentResponse empty() {
        return VendorParentResponse.builder()
                .vendorResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasVendorResponse() {
        return Objects.nonNull(getVendorResponse());
    }

    @Override
    public Vendor target() {
        if (hasVendorResponse())
            return vendorResponse.getVendor();
        return null;
    }

    @Override
    public ResponseStatus error() {
        if (hasErrorResponse())
            return errorResponse.getResponseStatus();
        return null;
    }

    @Override
    public Dependence dependence() {
        if (hasVendorResponse())
            return vendorResponse.getDependence();
        return null;
    }

    public String getVendorName() {
        if (hasVendorResponse())
            return vendorResponse.getVendor().getVendorName();
        return null;
    }

    public String getVendorId() {
        if (hasVendorResponse())
            return vendorResponse.getVendor().getVendorId();
        return null;
    }
}
