package com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.Returnable;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate.Delegate;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import lombok.*;

import java.io.Serializable;
import java.util.List;

import java.util.Objects;



@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LicenseAuthDelegatesParentResponse implements Serializable,
        Returnable<List<Delegate>, ResponseStatus, Dependence> {

    private static final long serialVersionUID = -2669997063759701334L;

    private ErrorResponse errorResponse;

    private LicenseAuthDelegatesResponse licenseAuthDelegatesResponse;

    private Integer httpResponseStatus;


    public static LicenseAuthDelegatesParentResponse empty() {
        return LicenseAuthDelegatesParentResponse.builder()
                .licenseAuthDelegatesResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasDelegatesResponse() {
        return Objects.nonNull(getLicenseAuthDelegatesResponse());
    }

    @Override
    public List<Delegate> target() {
        if (hasDelegatesResponse())
            return licenseAuthDelegatesResponse.getLicenseAuthDelegates().getDelegates();
        return null;
    }

    @Override
    public ResponseStatus error() {
        if (hasErrorResponse())
            errorResponse.getResponseStatus();
        return null;
    }

    @Override
    public Dependence dependence() {
        return null;
    }
}
