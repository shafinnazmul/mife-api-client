package com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Getter
@Setter
@XmlRootElement(name = "vendorCreateRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "Builder", toBuilder = true)
public class VendorCreateRequest implements Serializable {

    private static final long serialVersionUID = -7056010473283894438L;

    private String vendorName;
    private String aliasName;
    private Integer registableState;
    private Integer reachableState;
    private Integer chargeableState;
    private Integer demoVisible;
    private String sourceIpaddress;
    private String contactMail;
    private Integer maxQueryPercentage;
    private String description;
    private String descriptionUrl;
    private Integer moderatorApprovalState;
    private Integer vendorApprovalState;
    private Integer providerApprovalState;

    public VendorCreateRequest() {
    }

    public VendorCreateRequest(String vendorName,
                               String aliasName,
                               Integer registableState,
                               Integer reachableState,
                               Integer chargeableState,
                               Integer demoVisible,
                               String sourceIpaddress,
                               String contactMail,
                               Integer maxQueryPercentage,
                               String description,
                               String descriptionUrl,
                               Integer moderatorApprovalState,
                               Integer vendorApprovalState,
                               Integer providerApprovalState
    ) {
        this.vendorName = vendorName;
        this.aliasName = aliasName;
        this.registableState = registableState;
        this.reachableState = reachableState;
        this.chargeableState = chargeableState;
        this.demoVisible = demoVisible;
        this.sourceIpaddress = sourceIpaddress;
        this.contactMail = contactMail;
        this.description = description;
        this.descriptionUrl = descriptionUrl;
        this.moderatorApprovalState = moderatorApprovalState;
        this.vendorApprovalState = vendorApprovalState;
        this.providerApprovalState = providerApprovalState;
        this.maxQueryPercentage = maxQueryPercentage;
    }
}
