package com.mashfin.licenseissuer.mife.api.endpoint.dto.service;

import lombok.Builder;
import lombok.Getter;

import javax.ws.rs.QueryParam;
import java.io.Serializable;


@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
public class ServiceGetParams implements Serializable {

    private static final long serialVersionUID = -8250719565169025323L;

    private String serviceId;

    public ServiceGetParams(String serviceId) {
        this.serviceId = serviceId;
    }

    @QueryParam("serviceId")
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

}
