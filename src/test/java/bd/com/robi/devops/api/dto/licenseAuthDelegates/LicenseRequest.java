package com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@XmlAccessorType(XmlAccessType.FIELD)
public class LicenseRequest implements Serializable {

    private static final long serialVersionUID = -1400146255578805346L;

    private String licenseId;
}
