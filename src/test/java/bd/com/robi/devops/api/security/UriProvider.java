package com.mashfin.licenseissuer.mife.api.security;



public interface UriProvider {

    String serviceUri();
}
