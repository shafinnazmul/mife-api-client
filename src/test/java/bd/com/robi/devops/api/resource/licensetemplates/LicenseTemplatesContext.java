package bd.com.robi.devops.api.resource.licensetemplates;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.LicenseTemplatesResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.LicenseTemplatesResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"com.mashfin.licenseissuer.mife.api.resource"})
public class LicenseTemplatesContext extends BaseContext {
    @Bean
    public LicenseTemplatesResource licenseTemplatesResource() {
        return new LicenseTemplatesResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }
}
