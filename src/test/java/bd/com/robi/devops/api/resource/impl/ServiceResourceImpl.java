package com.mashfin.licenseissuer.mife.api.resource.impl;


import com.mashfin.licenseissuer.mife.api.support.Preconditions;
import bd.com.robi.devops.constant.LicenseIssuerResponseConstant;
import bd.com.robi.devops.exception.ApiException;
import bd.com.robi.devops.exception.SystemException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.service.*;
import com.mashfin.licenseissuer.mife.api.resource.ServiceResource;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import com.mashfin.licenseissuer.mife.api.support.JAXRSClientUtil;
import bd.com.robi.devops.support.ThrowableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import javax.el.MethodNotFoundException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Slf4j
public class ServiceResourceImpl implements ServiceResource {


    @Bean
    private ThrowableException throwable() {
        return new ThrowableException();
    }

    private ApiConnectionProvider authProvider;

    public ServiceResourceImpl(ApiConnectionProvider apiConnectionProvider) {

        this.authProvider = apiConnectionProvider;
    }

    private String serviceEndPoint(String providerUri) {

        return authProvider.serviceUri() + providerUri;
    }

    private String getURI() {
        return serviceEndPoint(("mife.operator.service.action"));
    }

    @Override
    public ServiceGetParentResponse getService(ServiceGetParams serviceGetParams, String requestId) throws ApiException {
        try {
            ServiceResponse response = JAXRSClientUtil.prepareResource(getURI(),
                    ServiceResourceImpl.ServiceResourceMapping.class,
                    authProvider).getService(serviceGetParams);
            return ServiceGetParentResponse
                    .builder()
                    .serviceResponse(response)
                    .httpResponseStatus(200)
                    .build();
        } catch (BadRequestException e) {
            Response response = e.getResponse();
            return ServiceGetParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }

    @Override
    public ServicePostParentResponse postService(ServicePostBody servicePostBody) {
        try {
            Preconditions.checkRequired(servicePostBody, "servicePostBody required!");
            try {
                ServiceCreateResponse response = JAXRSClientUtil.prepareResource(getURI(),
                        ServiceResourceImpl.ServiceResourceMapping.class,
                        authProvider).postService(servicePostBody);
                return ServicePostParentResponse
                        .builder()
                        .serviceCreateResponse(response)
                        .httpResponseStatus(200)
                        .build();
            } catch (NotAuthorizedException | BadRequestException ex) {
                Response response = ex.getResponse();
                return ServicePostParentResponse.builder()
                        .errorResponse(response.readEntity(ErrorResponse.class))
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();

            } catch (InternalServerErrorException | NotAllowedException | NotSupportedException | NotFoundException ex) {
                return ServicePostParentResponse.builder()
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();

            } catch (MethodNotFoundException | ProcessingException ex) {

                return ServicePostParentResponse.builder().build();

            }

        } catch (Exception e) {
            log.error("opps! what's happened ", e);
        }
        return null;
    }

    @Override
    public ServicePutParentResponse putService(ServicePutBody servicePutBody, String requestId) throws ApiException {
        try {
            ServiceUpdateResponse response = JAXRSClientUtil.prepareResource(getURI(),
                    ServiceResourceImpl.ServiceResourceMapping.class,
                    authProvider).putService(servicePutBody);
            return ServicePutParentResponse
                    .builder()
                    .serviceUpdateResponse(response)
                    .httpResponseStatus(200)
                    .build();
        } catch (BadRequestException e) {
            Response response = e.getResponse();
            return ServicePutParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }

    public interface ServiceResourceMapping {
        @GET
        ServiceResponse getService(@BeanParam ServiceGetParams serviceGetParams)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @POST
        ServiceCreateResponse postService(@NotNull ServicePostBody servicePostBody)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @PUT
        ServiceUpdateResponse putService(@NotNull ServicePutBody servicePutBody)
                throws ProcessingException, NotAuthorizedException, NullPointerException;
    }
}

