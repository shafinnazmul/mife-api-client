package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate;

import com.mashfin.licenseissuer.mife.api.entity.DateAdapter;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Delegate implements Serializable {

    private static final long serialVersionUID = -1540564371544499285L;

    private String delegateId;

    private String delegateName;

    private String aliasName;

    private String authenticateDesctiption;

    private String externalEndpoint;

    private String internalServer;

    private String internalContext;

    private String internalEndpoint;

    private String arguments;

    private String developperMail;

    private String creationRequestModeratorId;

    private String moderatorDelegateGroupId;

    private Integer registableState;

    private Integer reachableState;

    private Integer activeState;

    private Integer providerApprovalProcess;

    private Integer authLimitDays;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date createTime;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date updateTime;

}
