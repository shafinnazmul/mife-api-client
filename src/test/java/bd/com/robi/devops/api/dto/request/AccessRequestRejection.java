package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class AccessRequestRejection implements Serializable{

    private static final long serialVersionUID = -5052628281383454697L;
    @XmlElement(name = "accessRequests")
    private AccessRequestRejectionInfo.AccessRequestRejectionWrapper result;
}
