package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Builder(builderClassName = "Builder", toBuilder = true)
@AllArgsConstructor
@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class AccessRequestUpdate implements Serializable {
    private static final long serialVersionUID = -4778794882650277014L;
    @XmlElement(name = "accessRequests")
    private AccessRequestUpdateInfo.AccessRequestsWrapper body;

    public AccessRequestUpdate() {
        
    }
}
