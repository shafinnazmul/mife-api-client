package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup;


import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.ws.rs.QueryParam;
import java.io.Serializable;



@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
public class DelegateGroupsGetParams implements Serializable {

    private static final long serialVersionUID = 7974841754995432056L;

    @NotNull
    private String sortKey;
    @NotNull
    private String sortOrder;
    @NotNull
    private Integer offset;
    @NotNull
    private Integer hits;


    @QueryParam("sortKey")
    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

    @QueryParam("sortOrder")
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @QueryParam("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @QueryParam("hits")
    public void setHits(Integer hits) {
        this.hits = hits;
    }
}
