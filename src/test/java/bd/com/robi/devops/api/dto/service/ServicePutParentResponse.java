package com.mashfin.licenseissuer.mife.api.endpoint.dto.service;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.Returnable;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import lombok.*;

import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ServicePutParentResponse implements Serializable , Returnable<Service, ResponseStatus, Dependence> {

    private static final long serialVersionUID = 8101184498992576575L;

    private ErrorResponse errorResponse;

    private ServiceUpdateResponse serviceUpdateResponse;

    private Integer httpResponseStatus;


    public static ServiceGetParentResponse empty() {
        return ServiceGetParentResponse.builder()
                .serviceResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasServiceResponse() {
        return Objects.nonNull(getServiceUpdateResponse());
    }

    @Override
    public Service target() {
        if (hasServiceResponse())
            return serviceUpdateResponse.getService();
        return null;
    }

    @Override
    public ResponseStatus error() {
        if (hasErrorResponse())
            return errorResponse.getResponseStatus();
        return null;
    }

    @Override
    public Dependence dependence() {
        if (hasServiceResponse())
            return serviceUpdateResponse.getDependence();
        return null;
    }
}
