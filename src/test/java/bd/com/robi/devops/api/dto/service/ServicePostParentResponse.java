package com.mashfin.licenseissuer.mife.api.endpoint.dto.service;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import lombok.*;

import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ServicePostParentResponse extends ModeratorResponse implements Serializable {

    private static final long serialVersionUID = 8101184498992576575L;


    protected ErrorResponse errorResponse;

    protected Integer httpResponseStatus;

    private ServiceCreateResponse serviceCreateResponse;

    public static ServiceGetParentResponse empty() {
        return ServiceGetParentResponse.builder()
                .serviceResponse(null)
                .build();
    }


    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasServiceResponse() {
        return Objects.nonNull(getServiceCreateResponse());
    }
}
