package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;


@Getter
@Setter
@Builder(builderClassName = "Builder", toBuilder = true)
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceRequest implements Serializable {

    private static final long serialVersionUID = -6206269695244692234L;

    private String serviceId;
}
