package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "delegateagentcondResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class DelegateAgentCondResponse implements Serializable {

    private static final long serialVersionUID = -3266773793242483948L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "delegateagentcond")
    private DelegateAgentCond delegateAgentCond;


    public static DelegateAgentCondResponse empty() {
        return DelegateAgentCondResponse.builder()
                .delegateAgentCond(null)
                .responseStatus(null)
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(getDelegateAgentCond());
    }
}
