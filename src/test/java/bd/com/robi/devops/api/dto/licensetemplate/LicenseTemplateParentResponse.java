package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import lombok.*;

import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LicenseTemplateParentResponse implements Serializable {

    private static final long serialVersionUID = 1005796094697429836L;

    private ErrorResponse errorResponse;

    private LicenseTemplateResponse licenseTemplateResponse;

    private Integer httpResponseStatus;


    public static LicenseTemplateParentResponse empty() {
        return LicenseTemplateParentResponse.builder()
                .licenseTemplateResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasDelegateResponse() {
        return Objects.nonNull(this.getLicenseTemplateResponse());
    }
}
