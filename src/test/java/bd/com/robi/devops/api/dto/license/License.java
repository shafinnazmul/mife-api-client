package com.mashfin.licenseissuer.mife.api.endpoint.dto.license;

import com.mashfin.licenseissuer.mife.api.entity.DateAdapter;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class License implements Serializable {
    private static final long serialVersionUID = -1540564371544499285L;

    private String licenseId;

    private String licenseName;

    private String aliasName;

    private Integer registableState;

    private Integer reachableState;

    private Integer chargeableState;

    private Integer demoVisible;

    private String sourceIpAddress;

    private String contactMail;

    private Integer maxQueryPercentage;

    private String parentId;

    private String description;

    private String descriptionUrl;

    private Integer moderatorApprovalState;

    private Integer vendorApprovalState;

    private Integer vendorApprovalStateLockFlag;

    private Integer providerApprovalState;

    private Integer providerApprovalStateLockFlag;

    private String providerId;

    private Integer externalSystemId;

    private String externalProviderId;

    @XmlJavaTypeAdapter(DateAdapter.class)
    Date issueDate;

    @XmlJavaTypeAdapter(DateAdapter.class)
    Date useStartDate;

    @XmlJavaTypeAdapter(DateAdapter.class)
    Date expireDate;

    String clientEnvId;

    String key;

    @XmlJavaTypeAdapter(DateAdapter.class)
    Date createTime;

    @XmlJavaTypeAdapter(DateAdapter.class)
    Date updateTime;

}
