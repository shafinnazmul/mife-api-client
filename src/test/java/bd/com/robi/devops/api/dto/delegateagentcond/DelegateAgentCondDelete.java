package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond;

import lombok.Builder;
import lombok.Getter;

import javax.ws.rs.QueryParam;
import java.io.Serializable;



@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
public class DelegateAgentCondDelete implements Serializable {

    private static final long serialVersionUID = 379789433484618193L;

    private String agentId;

    private String delegateId;

    public DelegateAgentCondDelete(String agentId, String delegateId) {
        this.delegateId = delegateId;
        this.agentId = agentId;
    }

    @QueryParam("delegateId")
    public void setDelegateId(String delegateId) {
        this.delegateId = delegateId;
    }

    @QueryParam("agentId")
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

}
