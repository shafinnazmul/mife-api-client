package com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.LicenseTemplate;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;


@Getter
@Setter
@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class GeneratorLicenseTemplateRequest implements Serializable {
    private static final long serialVersionUID = 7394725165996275077L;
    private String licensetemplateId;
    private String licensetemplateName;
    private String aliasName;
    private String contactMail;
    private String description;
    private String descriptionUrl;
    private String downloadUrl;
    private Integer limitDay;

    public GeneratorLicenseTemplateRequest(LicenseTemplate licenseTemplate) {
        this.licensetemplateId = licenseTemplate.getLicensetemplateId();
        this.licensetemplateName = licenseTemplate.getLicensetemplateName();
        this.aliasName = licenseTemplate.getAliasName();
        this.contactMail = licenseTemplate.getContactMail();
        this.description = licenseTemplate.getDescription();
        this.descriptionUrl = licenseTemplate.getDescriptionUrl();
        this.downloadUrl = licenseTemplate.getDownloadUrl();
        this.limitDay = licenseTemplate.getLimitDays();

    }
}
