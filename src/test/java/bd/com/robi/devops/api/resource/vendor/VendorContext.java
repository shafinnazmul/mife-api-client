package bd.com.robi.devops.api.resource.vendor;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.VendorResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.VendorResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"bd.com.robi.devops.api.resource"})
public class VendorContext extends BaseContext {
    @Bean
    public VendorResource vendorRepository() {
        return new VendorResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }
}
