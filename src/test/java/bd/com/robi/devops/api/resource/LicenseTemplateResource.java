package com.mashfin.licenseissuer.mife.api.resource;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.LicenseTemplateParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.LicenseTemplatePost;


public interface LicenseTemplateResource {
    LicenseTemplateParentResponse addLicenseTemplate(LicenseTemplatePost licenseTemplatePost);

    LicenseTemplateParentResponse getLicenseTemplate(String licenseTemplateId);
}
