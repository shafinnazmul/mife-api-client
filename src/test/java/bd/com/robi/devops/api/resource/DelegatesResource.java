package com.mashfin.licenseissuer.mife.api.resource;

import bd.com.robi.devops.exception.ApiException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegates.DelegatesGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegates.DelegatesParentResponse;


public interface DelegatesResource {
    DelegatesParentResponse getDelegates(DelegatesGetParams delegatesGetParams, String requestId) throws ApiException;
}
