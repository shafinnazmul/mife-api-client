package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class AccessRequestApproval implements Serializable{

    private static final long serialVersionUID = -8562736329915374596L;
    @XmlElement(name = "accessRequests")
    private AccessRequestApprovalInfo.AccessRequestApprovalWrapper result;
}
