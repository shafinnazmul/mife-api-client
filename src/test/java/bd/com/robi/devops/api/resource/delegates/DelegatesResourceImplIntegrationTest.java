package bd.com.robi.devops.api.resource.delegates;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegates.DelegatesGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegates.DelegatesParentResponse;
import com.mashfin.licenseissuer.mife.api.resource.DelegatesResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DelegatesContext.class})
public class DelegatesResourceImplIntegrationTest {

    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private DelegatesResource delegatesResource;

    @Test
    public void getDelegatesErrorResponseTest() throws Exception {
        DelegatesParentResponse delegatesParentResponse = delegatesResource.getDelegates(
                new DelegatesGetParams("abcd",
                        "ascending",
                        0,
                        10,
                        ""), "requestId");
        Assert.notNull(delegatesParentResponse.getErrorResponse());
    }

    @Test
    public void getDelegatesValidResponseTest() throws Exception {
        DelegatesParentResponse delegateParentResponse = delegatesResource.getDelegates(
                new DelegatesGetParams(
                        "delegate.delegateId",
                        "ascending", 0, 10,
                        "05f6dda0-6ca0-11e5-bced-0019b9e60f26"), "requestId");
        Assert.notNull(delegateParentResponse.getDelegatesResponse().getDelegates());
    }
}
