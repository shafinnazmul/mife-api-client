package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import lombok.*;

import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LicenseTemplatesParentResponse implements Serializable {

    private static final long serialVersionUID = -7118703590492290956L;

    private ErrorResponse errorResponse;

    private LicenseTemplatesResponse licenseTemplateResponse;

    private Integer httpResponseStatus;


    public static LicenseTemplatesParentResponse empty() {
        return LicenseTemplatesParentResponse.builder()
                .licenseTemplateResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasDelegateResponse() {
        return Objects.nonNull(this.getLicenseTemplateResponse());
    }
}
