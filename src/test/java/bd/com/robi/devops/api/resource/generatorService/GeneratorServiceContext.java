package bd.com.robi.devops.api.resource.generatorService;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.GeneratorServiceResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.GeneratorServiceResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"bd.com.robi.devops.api.resource"})
public class GeneratorServiceContext extends BaseContext {
    @Bean
    public GeneratorServiceResource serviceRepository() {
        return new GeneratorServiceResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }
}
