package com.mashfin.licenseissuer.mife.api.endpoint.dto.license;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.io.Serializable;

@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class LicensesGetParam implements Serializable {

    private static final long serialVersionUID = -8308094566669599215L;

    private String sortKey;

    private String sortOrder;

    private String sortKey2;

    private String sortOrder2;

    private Integer offset;

    private Integer hits;

    private String vendorId;

    private String providerId;

    private String licensetemplateId;

    private String serviceId;

    private Integer externalSystemId;

    private String externalProviderId;

    private String groupingKey;

    private String extractKey;

    private String extractOrder;

    @QueryParam("sortKey")
    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

    @QueryParam("sortOrder")
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @QueryParam("sortKey2")
    public void setSortKey2(String sortKey2) {
        this.sortKey2 = sortKey2;
    }

    @QueryParam("sortOrder2")
    public void setSortOrder2(String sortOrder2) {
        this.sortOrder2 = sortOrder2;
    }

    @QueryParam("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @QueryParam("hits")
    public void setHits(Integer hits) {
        this.hits = hits;
    }

    @QueryParam("vendorId")
    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    @QueryParam("providerId")
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    @QueryParam("licensetemplateId")
    public void setLicensetemplateId(String licensetemplateId) {
        this.licensetemplateId = licensetemplateId;
    }

    @QueryParam("serviceId")
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    @QueryParam("externalSystemId")
    public void setExternalSystemId(Integer externalSystemId) {
        this.externalSystemId = externalSystemId;
    }

    @QueryParam("externalProviderId")
    public void setExternalProviderId(String externalProviderId) {
        this.externalProviderId = externalProviderId;
    }

    @QueryParam("groupingKey")
    public void setGroupingKey(String groupingKey) {
        this.groupingKey = groupingKey;
    }

    @QueryParam("extractKey")
    public void setExtractKey(String extractKey) {
        this.extractKey = extractKey;
    }

    @QueryParam("extractOrder")
    public void setExtractOrder(String extractOrder) {
        this.extractOrder = extractOrder;
    }
}
