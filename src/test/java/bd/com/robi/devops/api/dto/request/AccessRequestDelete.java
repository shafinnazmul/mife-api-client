package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class AccessRequestDelete {
    @XmlElement(name = "accessRequests")
    private AccessRequestDeleteInfo.AccessRequestDeleteWrapper result;
}
