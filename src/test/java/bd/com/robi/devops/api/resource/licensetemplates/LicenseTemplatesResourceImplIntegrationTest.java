package bd.com.robi.devops.api.resource.licensetemplates;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates.LicenseTemplatesGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates.LicenseTemplatesParentResponse;
import com.mashfin.licenseissuer.mife.api.resource.LicenseTemplatesResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {LicenseTemplatesContext.class})
public class LicenseTemplatesResourceImplIntegrationTest {

    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private LicenseTemplatesResource licenseTemplatesResource;

    @Test
    public void getLicenseTemplatesValidResponseTest() {
        LicenseTemplatesParentResponse response = licenseTemplatesResource.getLicenseTemplates(
                new LicenseTemplatesGetParams("licensetemplate.aliasName",
                        "ascending", 0, 30,"e568afc0-3eeb-11e8-af75-425732242c5c"));
        Assert.notNull(response.getLicenseTemplateResponse().getLicenseTemplates());
    }

    @Test
    public void getLicenseTemplatesErrorResponseTest() throws Exception {
        LicenseTemplatesParentResponse response = licenseTemplatesResource.getLicenseTemplates(
                new LicenseTemplatesGetParams("abcd", "ascending", 0,
                        30,"9c2bc160-3c9f-11e8-af75-425732242c5c"));
        Assert.notNull(response.getErrorResponse());
    }

}
