package bd.com.robi.devops.api.resource.delegate;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate.DelegateGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate.DelegateParentResponse;
import com.mashfin.licenseissuer.mife.api.resource.DelegateResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DelegateContext.class})
public class DelegateResourceImplIntegrationTest {

    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private DelegateResource delegateResource;

    @Test
    public void getDelegateErrorResponseTest() throws Exception {
        DelegateParentResponse delegateParentResponse = delegateResource.getDelegate(
                new DelegateGetParams("04"), "requestId");
        Assert.notNull(delegateParentResponse.getErrorResponse());
    }

    @Test
    public void getDelegateValidResponseTest() throws Exception {
        DelegateParentResponse delegateParentResponse = delegateResource.getDelegate(
                new DelegateGetParams("0051dd10-e221-11e3-9790-0019b9e60f26"), "requestId");
        Assert.notNull(delegateParentResponse.getDelegateResponse().getDelegate());
    }


}
