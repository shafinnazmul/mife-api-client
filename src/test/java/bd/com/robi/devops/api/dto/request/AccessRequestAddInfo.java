package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;


@Setter
@Getter
@Builder(builderClassName = "AccessRequestBuilder", toBuilder = true, builderMethodName = "hiddenBuilder")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessRequestAddInfo implements Serializable {
    private static final long serialVersionUID = -4599377332932949317L;


    @NotNull
    private String delegateId;

    @NotNull
    private String serviceId;

    @NotNull
    private String licenseId;

    @NotNull
    private String requesterAccount;

    @NotNull
    private String requesterEmail;

    @NotNull
    private String agentId;

    @NotNull
    private String delegateGroupId;

    private String delegateCondId;

    private String providerId;

    private Integer maxQps;
    private Integer maxQpm;
    private Integer maxConnection;
    private Integer responseTimeOut;


    public static AccessRequestBuilder builder(String delegateId, String serviceId,
                                               String licenseId, String requesterAccount, String requesterEmail,
                                               String agentId, String delegateGroupId) {
        return hiddenBuilder()
                .delegateId(delegateId)
                .serviceId(serviceId)
                .licenseId(licenseId)
                .requesterAccount(requesterAccount)
                .requesterEmail(requesterEmail)
                .agentId(agentId)
                .delegateGroupId(delegateGroupId);


    }


    @AllArgsConstructor
    @Getter
    @Setter
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AccessRequestsWrapper implements Serializable {

        private static final long serialVersionUID = -1254936532585739129L;
        @XmlElement(name = "accessRequest")
        private List<AccessRequestAddInfo> items;





    }


}
