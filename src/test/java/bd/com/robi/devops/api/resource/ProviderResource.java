package com.mashfin.licenseissuer.mife.api.resource;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.provider.ProviderParam;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.provider.ProviderParentResponse;


public interface ProviderResource {
    ProviderParentResponse getProvider(ProviderParam providerParam);
}
