package com.mashfin.licenseissuer.mife.api.endpoint.dto.service;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;


@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "Builder", toBuilder = true)
public class ServicePutRequest implements Serializable {

    private static final long serialVersionUID = 793187444338539005L;

    private String serviceId;
    private String serviceName;
    private String aliasName;
    private Integer registableState;
    private Integer reachableState;
    private Integer chargeableState;
    private Integer demoVisible;
    private String sourceIpaddress;
    private Integer maxQueryPercentage;
    private String contactMail;
    private String description;
    private String descriptionUrl;
    private Integer moderatorApprovalState;
    private Integer vendorApprovalState;
    private Integer providerApprovalState;

    /*public ServicePutRequest() {
    }*/

    /*public ServicePutRequest(
            String serviceId,
            String serviceName,
            String aliasName,
            Integer registableState,
            Integer reachableState,
            Integer chargeableState,
            Integer demoVisible,
            String sourceIpaddress,
            Integer maxQueryPercentage,
            String contactMail,
            String description,
            String descriptionUrl,
            Integer moderatorApprovalState,
            Integer vendorApprovalState,
            Integer providerApprovalState,
            String prefix) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.aliasName = aliasName;
        this.registableState = registableState;
        this.reachableState = reachableState;
        this.chargeableState = chargeableState;
        this.demoVisible = demoVisible;
        this.sourceIpaddress = sourceIpaddress;
        this.contactMail = contactMail;
        this.maxQueryPercentage = maxQueryPercentage;
        this.description = description;
        this.descriptionUrl = descriptionUrl;
        this.moderatorApprovalState = moderatorApprovalState;
        this.vendorApprovalState = vendorApprovalState;
        this.providerApprovalState = providerApprovalState;
        this.prefix = prefix;
    }*/
}
