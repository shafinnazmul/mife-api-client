package com.mashfin.licenseissuer.mife.api.resource.impl;

import com.mashfin.licenseissuer.mife.api.support.Preconditions;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates.LicenseTemplatesGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates.LicenseTemplatesParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates.LicenseTemplatesResponse;
import com.mashfin.licenseissuer.mife.api.resource.LicenseTemplatesResource;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import com.mashfin.licenseissuer.mife.api.support.JAXRSClientUtil;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.*;


@Slf4j
public class LicenseTemplatesResourceImpl implements LicenseTemplatesResource {
    public static final String LICENSE_TEMPLATES_GET_URI = "/moderator/operator/licensetemplates";

    private ApiConnectionProvider authProvider;

    public LicenseTemplatesResourceImpl(ApiConnectionProvider apiConnectionProvider) {
        this.authProvider = apiConnectionProvider;
    }

    public String serviceEndPoint(String url) {
        return authProvider.serviceUri() + url;
    }

    @Override
    public LicenseTemplatesParentResponse getLicenseTemplates(LicenseTemplatesGetParams licenseTemplatesGetParams) {
        try {
            checkRequiredParams(licenseTemplatesGetParams);
            try {
                LicenseTemplatesResponse licenseTemplatesResponse = JAXRSClientUtil.prepareResource(
                        serviceEndPoint(LICENSE_TEMPLATES_GET_URI),
                        LicenseTemplatesResourceMapping.class,
                        authProvider).getLicenseTemplates(licenseTemplatesGetParams);

                return LicenseTemplatesParentResponse.builder()
                        .licenseTemplateResponse(licenseTemplatesResponse)
                        .httpResponseStatus(200)
                        .build();
            } catch (NotAuthorizedException | BadRequestException ex) {
                return LicenseTemplatesParentResponse.builder()
                        .errorResponse(ex.getResponse().readEntity(ErrorResponse.class))
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();
            } catch (InternalServerErrorException | NotAllowedException | NotSupportedException | NotFoundException ex) {
                return LicenseTemplatesParentResponse.builder()
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();
            } catch (NullPointerException ex) {
                return LicenseTemplatesParentResponse.builder().build();
            }
        } catch (Exception e) {
            log.error("opps! what's happened ", e);
        }
        return null;
    }

    private void checkRequiredParams(LicenseTemplatesGetParams licenseTemplatesGetParams) {
        Preconditions.checkRequired(licenseTemplatesGetParams, "License Templates params are required.");
        Preconditions.checkRequired(licenseTemplatesGetParams.getSortKey(), "SortKey param is required.");
        Preconditions.checkRequired(licenseTemplatesGetParams.getSortOrder(), "SortOrder param is required.");
        Preconditions.checkRequired(licenseTemplatesGetParams.getOffset(), "Offset param is required.");
        Preconditions.checkRequired(licenseTemplatesGetParams.getHits(), "Hits param is required.");

    }

    public interface LicenseTemplatesResourceMapping {
        @GET
        LicenseTemplatesResponse getLicenseTemplates(@BeanParam LicenseTemplatesGetParams licenseTemplatesGetParams)
                throws ProcessingException, NotAuthorizedException, NullPointerException;
    }
}
