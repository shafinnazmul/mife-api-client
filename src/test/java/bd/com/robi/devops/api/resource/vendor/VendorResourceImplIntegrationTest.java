package bd.com.robi.devops.api.resource.vendor;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.VendorCreateRequest;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.VendorGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.VendorParentResponse;
import com.mashfin.licenseissuer.mife.api.resource.VendorResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.UUID;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {VendorContext.class})
public class VendorResourceImplIntegrationTest {
    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private VendorResource vendorResource;

    @Test
    public void getVendorTest() throws Exception {
        VendorParentResponse vendorParentResponse = vendorResource.getVendor(
                VendorGetParams
                        .builder()
                        .vendorId("aea47e40-2d8b-11e8-af75-425732242c5c")
                        .build(), UUID.randomUUID().toString());
        Assert.notNull(vendorParentResponse.getVendorResponse());
    }

    @Test
    public void getVendorNameTest() throws Exception {
        VendorParentResponse vendorParentResponse = vendorResource.getVendor(
                VendorGetParams
                        .builder()
                        .vendorName("RA010")
                        .build(), UUID.randomUUID().toString());
        Assert.notNull(vendorParentResponse.getVendorResponse());
    }

    @Test
    public void postVendorTest() throws Exception {
        VendorParentResponse vendorParentResponse = vendorResource.postVendor(
                VendorCreateRequest.builder()
                        .vendorName(UUID.randomUUID().toString())
                        .aliasName("test11")
                        .registableState(1)
                        .reachableState(1)
                        .chargeableState(1)
                        .demoVisible(0)
                        .sourceIpaddress("")
                        .contactMail("marie@gmail.com")
                        .maxQueryPercentage(100)
                        .description("For test purpose")
                        .descriptionUrl("")
                        .moderatorApprovalState(2)
                        .vendorApprovalState(2)
                        .providerApprovalState(2)
                        .build(), UUID.randomUUID().toString());
        Assert.notNull(vendorParentResponse.getVendorResponse());
    }
}
