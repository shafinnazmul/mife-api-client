package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates;

import com.mashfin.licenseissuer.mife.api.entity.DateAdapter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;



@Getter
@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
public class LicenseTemplatesGetParams implements Serializable {

    private static final long serialVersionUID = -2795707827702194540L;
    private String sortKey;

    private String sortOrder;

    private Integer offset;

    private Integer hits;

    private String vendorId;

    private String serviceId;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date updateFrom;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date updateTo;

    public LicenseTemplatesGetParams(String sortKey, String sortOrder,
                                     Integer offset, Integer hits, String serviceId) {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.offset = offset;
        this.hits = hits;
        this.serviceId = serviceId;
    }

    @QueryParam("sortKey")
    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

    @QueryParam("sortOrder")
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @QueryParam("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @QueryParam("hits")
    public void setHits(Integer hits) {
        this.hits = hits;
    }

    @QueryParam("vendorId")
    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    @QueryParam("serviceId")
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    @QueryParam("updateFrom")
    public void setUpdateFrom(Date updateFrom) {
        this.updateFrom = updateFrom;
    }

    @QueryParam("updateTo")
    public void setUpdateTo(Date updateTo) {
        this.updateTo = updateTo;
    }
}
