package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegates;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate.Delegate;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class Delegates implements Serializable {

    private static final long serialVersionUID = 5991566184959428476L;

    @XmlElement(name = "delegate")
    private List<Delegate> delegates;


}
