package com.mashfin.licenseissuer.mife.api.endpoint.dto.service;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Getter
@Setter
@XmlRootElement(name = "vendorRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "Builder", toBuilder = true)
public class VendorRequest implements Serializable {

    private static final long serialVersionUID = 6217171628051388521L;

    private String vendorId;

    public VendorRequest(){}

    public VendorRequest(String vendorId) {
        this.vendorId = vendorId;
    }
}
