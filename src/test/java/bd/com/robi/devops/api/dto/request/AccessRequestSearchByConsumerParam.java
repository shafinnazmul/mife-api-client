package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.Builder;
import lombok.Getter;

import javax.ws.rs.QueryParam;


@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
public class AccessRequestSearchByConsumerParam {

    private String licenseId;

    private String requestDateFrom;

    private String requestDateTo;

    private Integer status;

    private String delegateId;

    private String delegateGroupId;

    private String approvalDateFrom;

    private String approvalDateTo;

    private Integer maxQps;

    private Integer maxQpm;

    private Integer maxConnection;


    @QueryParam("licenseId")
    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    @QueryParam("requestDateFrom")
    public void setRequestDateFrom(String requestDateFrom) {
        this.requestDateFrom = requestDateFrom;
    }

    @QueryParam("requestDateTo")
    public void setRequestDateTo(String requestDateTo) {
        this.requestDateTo = requestDateTo;
    }

    @QueryParam("delegateGroupId")
    public void setDelegateGroupId(String delegateGroupId) {
        this.delegateGroupId = delegateGroupId;
    }

    @QueryParam("delegateId")
    public void setDelegateId(String delegateId) {
        this.delegateId = delegateId;
    }


    @QueryParam("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @QueryParam("approvalDateFrom")
    public void setApprovalDateFrom(String approvalDateFrom) {
        this.approvalDateFrom = approvalDateFrom;
    }

    @QueryParam("approvalDateTo")
    public void setApprovalDateTo(String approvalDateTo) {
        this.approvalDateTo = approvalDateTo;
    }

    @QueryParam("maxQps")
    public void setMaxQps(Integer maxQps) {
        this.maxQps = maxQps;
    }

    @QueryParam("maxQpm")
    public void setMaxQpm(Integer maxQpm) {
        this.maxQpm = maxQpm;
    }

    @QueryParam("maxConnection")
    public void setMaxConnection(Integer maxConnection) {
        this.maxConnection = maxConnection;
    }
}
