package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;


@Getter
@Setter
@Builder(builderClassName = "Builder", toBuilder = true)
@XmlAccessorType(XmlAccessType.FIELD)
public class LicenseTemplateRequest implements Serializable {

    private static final long serialVersionUID = 5352535771910974456L;

    private String licensetemplateName;
    private String aliasName;
    private Integer registableState;
    private Integer reachableState;
    private Integer chargeableState;
    private Integer demoVisible;
    private String sourceIpaddress;
    private String contactMail;
    private Integer maxQueryPercentage;
    private String description;
    private String descriptionUrl;
    private String downloadUrl;
    private Integer moderatorApprovalState;
    private Integer vendorApprovalState;
    private Integer providerApprovalState;
    private Integer limitDay;

}
