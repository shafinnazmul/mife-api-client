package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "Builder", toBuilder = true)
public class DelegateAgentCondRequest implements Serializable {
    private static final long serialVersionUID = -7311555795155754839L;

    String delegateId;
    String agentId;
    Integer maxQps;
    Integer maxQpm;
    Integer maxConnection;

}
