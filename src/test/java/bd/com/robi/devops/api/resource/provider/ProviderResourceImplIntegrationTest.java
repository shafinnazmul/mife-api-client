package bd.com.robi.devops.api.resource.provider;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.provider.ProviderParam;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.provider.ProviderParentResponse;
import com.mashfin.licenseissuer.mife.api.resource.ProviderResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ProviderContext.class})
public class ProviderResourceImplIntegrationTest {

    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private ProviderResource providerResource;

    @Test
    public void getProviderTest() throws Exception {
        ProviderParentResponse providerResponse = providerResource.getProvider(
                new ProviderParam("0", "_payeasytest7"));
        Assert.notNull(providerResponse.getErrorResponse());
    }
}
