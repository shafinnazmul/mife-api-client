package com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.service.VendorRequest;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Getter
@Setter
@XmlRootElement(name = "generatorServiceRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class GeneratorServiceRequestBody implements Serializable {

    private static final long serialVersionUID = 2095933880005196247L;

    @XmlElement(name = "serviceRequest")
    private GeneratorServiceRequest generatorServiceRequest;

    @XmlElement(name = "vendorRequest")
    private VendorRequest vendorRequest;

    @XmlElement(name = "licensetemplateRequest")
    private GeneratorLicenseTemplateRequest generatorLicenseTemplateRequest;

    private DelegateIdsRequest delegateIdsRequest;
}
