package com.mashfin.licenseissuer.mife.api.endpoint.dto.service;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "serviceResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class ServiceResponse implements Serializable {

    private static final long serialVersionUID = -7850278859029651634L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "service")
    private Service service;
    
    private Dependence dependence;

    public static ServiceResponse empty() {
        return ServiceResponse.builder()
                .service(null)
                .responseStatus(null)
                .dependence(null)
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(getService());
    }
}
