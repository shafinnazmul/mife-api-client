package com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates;

import lombok.Builder;
import lombok.Getter;

import javax.ws.rs.QueryParam;
import java.io.Serializable;



@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
public class LicenseAuthDelegatesGetParams implements Serializable {

    private static final long serialVersionUID = -6126667088789404125L;

    private String sortKey;
    private String sortOrder;
    private Integer offset;
    private Integer hits;
    private String licenseId;

    public LicenseAuthDelegatesGetParams(String sortKey, String sortOrder,
                                         Integer offset, Integer hits,
                                         String licenseId) {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.offset = offset;
        this.hits = hits;
        this.licenseId = licenseId;
    }

    @QueryParam("sortKey")
    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

    @QueryParam("sortOrder")
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @QueryParam("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @QueryParam("hits")
    public void setHits(Integer hits) {
        this.hits = hits;
    }

    @QueryParam("licenseId")
    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }
}
