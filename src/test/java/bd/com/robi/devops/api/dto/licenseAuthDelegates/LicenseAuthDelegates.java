package com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate.Delegate;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicenseAuthDelegates implements Serializable {

    private static final long serialVersionUID = 8819559848433992671L;
    @XmlElement(name = "delegate")
    private List<Delegate> delegates;
}
