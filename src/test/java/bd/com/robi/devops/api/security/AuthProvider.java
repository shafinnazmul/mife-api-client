package com.mashfin.licenseissuer.mife.api.security;



public interface AuthProvider {
    AuthInfo authInfo();
}
