package com.mashfin.licenseissuer.mife.api.endpoint.dto.license;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import java.util.Objects;

@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "licenseResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicenseResponse implements Serializable {
    private static final long serialVersionUID = -200785515528734646L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "license")
    private License license;

    @XmlElement(name = "dependence")
    private Dependence dependence;

    public boolean hasResult() {
        return Objects.nonNull(getLicense());
    }
}
