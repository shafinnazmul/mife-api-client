package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;


@Setter
@Getter
@Builder(builderClassName = "AccessRequestBuilder", toBuilder = true, builderMethodName = "hiddenBuilder")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessRequestUpdateInfo implements Serializable {


    private static final long serialVersionUID = 7976198522682662839L;

    @NotNull
    private String requestId;

    @NotNull
    private String requesterAccount;

    @NotNull
    private Integer status;


    @NotNull
    private String requesterEmail;

    @NotNull
    private Integer action;

    private Integer maxQps;


    private Integer maxQpm;


    private Integer maxConnection;


    private Integer responseTimeOut;


    private String rejectionReason;

    private String approverAccount;


    public static AccessRequestBuilder builder(String requestId, String requesterAccount,
                                               Integer status, String requesterEmail,
                                               Integer action) {
        return hiddenBuilder()
                .requestId(requestId)
                .requesterAccount(requesterAccount)
                .status(status)
                .requesterEmail(requesterEmail)
                .requesterEmail(requesterEmail)
                .action(action);
    }


    @AllArgsConstructor
    @Getter
    @Setter
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AccessRequestsWrapper implements Serializable {

        private static final long serialVersionUID = 8854049653635596636L;
        @XmlElement(name = "accessRequest")
        private List<AccessRequestUpdateInfo> items;


    }


}
