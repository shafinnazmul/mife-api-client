package bd.com.robi.devops.api.resource.mife.operator.license;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.LicenseResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.LicenseResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(value = {"bd.com.robi.devops.api.resource"})
@Configuration
public class ModeratorOperatorLicenseContext extends BaseContext{

    @Bean
    LicenseResource moderatorOperatorLicenseResource(){
        return new LicenseResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }
}
