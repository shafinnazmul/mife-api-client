package com.mashfin.licenseissuer.mife.api.endpoint.dto;


public interface Returnable<T, E, D> {
    T target();

    E error();

    D dependence();
}
