package com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class VendorIds implements Serializable {

    private static final long serialVersionUID = 940873828725981001L;

    private String[] vendorId;
}
