package com.mashfin.licenseissuer.mife.api.resource;

import bd.com.robi.devops.exception.ApiException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.license.*;

public interface LicenseResource {
    LicenseParentResponse getLicense(String licenseId, String requestId) throws ApiException;

    LicensesParentResponse getLicenses(LicensesGetParam param, String requestId) throws ApiException;

    LicenseParentResponse addLicense(LicensePostRequest param, String requestId) throws ApiException;

    LicenseParentResponse updateLicense(LicensesPutRequest param, String requestId) throws ApiException;

    LicenseParentResponse getLicense(String licenseId, String key, String requestId) throws ApiException;

}
