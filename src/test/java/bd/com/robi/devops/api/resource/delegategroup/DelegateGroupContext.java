package bd.com.robi.devops.api.resource.delegategroup;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.DelegateGroupsResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.DelegateGroupsResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"bd.com.robi.devops.api.resource"})
public class DelegateGroupContext extends BaseContext {

    @Bean
    public DelegateGroupsResource delegateGroupsResource() {
        return new DelegateGroupsResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }

}
