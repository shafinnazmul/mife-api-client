package bd.com.robi.devops.api.resource.delegate;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.DelegateResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.DelegateResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"com.mashfin.licenseissuer.mife.api.resource"})
public class DelegateContext extends BaseContext {

    @Bean
    public DelegateResource delegateResource() {
        return new DelegateResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }

}
