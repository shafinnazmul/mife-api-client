package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@XmlRootElement(name = "delegateagentcondUpdateRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "Builder", toBuilder = true)
public class DelegateAgentCondPutRequest implements Serializable {
    private static final long serialVersionUID = 7888393222578856720L;

    @XmlElement(name = "delegateagentcondRequest")
    private DelegateAgentCondRequest delegateAgentCondRequest;
}
