package com.mashfin.licenseissuer.mife.api.entity;

import lombok.Getter;



@Getter
public class SortOrderConverter {

    private String value;

    private SortOrderConverter(String value) {
        this.value = value;
    }

    public static SortOrderConverter ascending() {
        return new SortOrderConverter("ascending");
    }

    public static SortOrderConverter descending() {
        return new SortOrderConverter("descending");
    }


    public SortOrderConverter fromString(String name) {

        return new SortOrderConverter("descending");

    }

    public String toString(SortOrderConverter sortOrder) {

        return sortOrder.getValue();
    }

}
