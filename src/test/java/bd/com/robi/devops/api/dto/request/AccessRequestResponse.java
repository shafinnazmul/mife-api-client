package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.Objects;

import java.util.Collections;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class AccessRequestResponse {
    @XmlElement(name = "accessRequests")
    private AccessRequestInfo.AccessRequestWrapper result;


    public static AccessRequestResponse empty() {
        return AccessRequestResponse
                .builder()
                .result(
                        AccessRequestInfo.AccessRequestWrapper.
                                builder()
                                .items (Collections.EMPTY_LIST)
                                .build())
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(getResult()) && !Objects.isNull(getResult().getItems());
    }
}
