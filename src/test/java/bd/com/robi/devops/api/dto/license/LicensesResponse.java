package com.mashfin.licenseissuer.mife.api.endpoint.dto.license;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

import java.util.Objects;

@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "licensesResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicensesResponse implements Serializable {
    private static final long serialVersionUID = -200785515528734646L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "totalCount")
    private Integer totalCount;

    @XmlElement(name = "licenses")
    private ModeratorOperatorLicenses licenses;

    @XmlElement(name = "dependences")
    public ModeratorOperatorLicenseDependences dependences;

    public boolean hasResult() {
        return Objects.nonNull(getLicenses());
    }

    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @XmlAccessorType(XmlAccessType.FIELD)
    @Getter
    @Setter
    public static class ModeratorOperatorLicenses implements Serializable {
        private static final long serialVersionUID = -2003776540647117778L;

        @XmlElement(name = "license")
        private List<License> licenseList;
    }

    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @XmlAccessorType(XmlAccessType.FIELD)
    @Getter
    @Setter
    public static class ModeratorOperatorLicenseDependences {
        @XmlElement(name = "dependence")
        private List<LicenseDependence> licenseDependences;
    }
}
