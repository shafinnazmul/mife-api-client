package com.mashfin.licenseissuer.mife.api.security;

import lombok.Builder;
import lombok.Getter;

import java.util.Base64;




@Getter
public class AuthInfo {

    private final String requestor;

    private final String authorization;

    private final String contentType;

    @Builder
    public AuthInfo(String requestor, String authorization, String contentType) {
        this.requestor = requestor;
        this.authorization = authorization;
        this.contentType = contentType;
    }

    public static AuthInfo prepare(String requestorType, String requestorId, String requestorPassword,
                                   String apiId, String apiPassword, String contentType) {
        return AuthInfo.builder()
                .requestor(resolveRequestor(requestorType, requestorId, requestorPassword))
                .authorization(resolveAuthorization(apiId, apiPassword))
                .contentType(contentType)
                .build();
    }

    private static String resolveAuthorization(String apiId, String apiPassword) {
        String temp = apiId + ":" + apiPassword;
        String encoded = "basic " + Base64.getEncoder().encodeToString(temp.getBytes());
        return encoded;
    }


    static public String resolveRequestor(String requestorType, String requestorId, String requestorPassword) {
        String temp = requestorType + ":" + requestorId + ":" + requestorPassword;
        String encoded = Base64.getEncoder().encodeToString(temp.getBytes());
        return encoded;
    }

}
