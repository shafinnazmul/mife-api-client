package com.mashfin.licenseissuer.mife.api.endpoint.dto.license;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.Returnable;
import lombok.*;

import java.io.Serializable;

import java.util.Objects;

@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LicensesParentResponse implements Serializable, Returnable<LicensesResponse.ModeratorOperatorLicenses, ResponseStatus, LicensesResponse.ModeratorOperatorLicenseDependences> {
    private static final long serialVersionUID = 6380729401026241337L;

    private ErrorResponse errorResponse;

    private LicensesResponse licensesResponse;

    Integer httpResponseStatus;

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasLicensesResponse() {
        return Objects.nonNull(this.getLicensesResponse());
    }

    @Override
    public LicensesResponse.ModeratorOperatorLicenses target() {
        if (hasLicensesResponse())
            return licensesResponse.getLicenses();
        return null;
    }

    @Override
    public ResponseStatus error() {
        if (hasErrorResponse())
            errorResponse.getResponseStatus();
        return null;
    }

    @Override
    public LicensesResponse.ModeratorOperatorLicenseDependences dependence() {
        if (hasLicensesResponse())
            return licensesResponse.getDependences();
        return null;
    }

    public Integer getTotalCount() {
        if (hasLicensesResponse())
            return licensesResponse.getTotalCount();
        return null;
    }
}