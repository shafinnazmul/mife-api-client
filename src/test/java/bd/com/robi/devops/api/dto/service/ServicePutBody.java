package com.mashfin.licenseissuer.mife.api.endpoint.dto.service;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Getter
@Setter
@XmlRootElement(name = "serviceUpdateRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "Builder", toBuilder = true)
public class ServicePutBody implements Serializable {

    private static final long serialVersionUID = -5661143250815640218L;

    @XmlElement(name = "serviceRequest")
    private ServicePutRequest servicePutRequest;

    public ServicePutBody() {
    }

    public ServicePutBody(ServicePutRequest servicePutRequest) {
        this.servicePutRequest = servicePutRequest;
    }
}
