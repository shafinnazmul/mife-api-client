package com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@XmlAccessorType(XmlAccessType.FIELD)
public class DelegateIdsRequest implements Serializable {

    private static final long serialVersionUID = 2729330894114023611L;

    @XmlElement(name = "delegateId")
    private List<String> delegateIds;
}
