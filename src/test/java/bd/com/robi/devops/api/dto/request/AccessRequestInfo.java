package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;


import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessRequestInfo implements Serializable {

    private static final long serialVersionUID = 8325406887058231309L;
    @XmlElement(name = "requestId")
    private String accessRequestId;
    private String delegateCondId;
    private String serviceId;
    private String licenseId;
    private Integer maxQps;
    private Integer maxQpm;
    private Integer maxConnection;
    private Date requestTime;
    private Date updateDate;
    private String requesterAccount;
    private Integer status;
    private String requesterEmail;
    private String rejectionReason;
    private String approverAccount;
    private String providerId;
    private String delegateId;
    private Integer responseTimeOut;
    private Integer action;
    private String delegateName;
    private String delegateGroupName;
    private String vendorId;
    private String delegateGroupId;
    private String consumerGroup;
    private String consumerAccount;
    private String authKey;
    private String serviceName;
    private String providerGroup;



    @Builder(builderClassName = "Builder", toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AccessRequestWrapper implements Serializable {

        private static final long serialVersionUID = -8229994710038111287L;

        @XmlElement(name = "accessRequest")
        private List<AccessRequestInfo> items;
    }

}
