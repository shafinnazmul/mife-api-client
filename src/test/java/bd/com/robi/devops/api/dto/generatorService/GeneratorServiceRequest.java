package com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.service.Service;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
public class GeneratorServiceRequest {
    private static final long serialVersionUID = 3837541579955441286L;
    private String serviceId;
    private String serviceName;
    private String aliasName;
    private String contactMail;
    private String description;
    private String descriptionUrl;
    private String prefix;

    public GeneratorServiceRequest(Service service) {
        this.serviceId = service.getServiceId();
        this.serviceName = service.getServiceName();
        this.aliasName = service.getAliasName();
        this.contactMail = service.getContactMail();
        this.description = service.getDescription();
        this.descriptionUrl = service.getDescriptionUrl();
        this.prefix = "";
    }
}
