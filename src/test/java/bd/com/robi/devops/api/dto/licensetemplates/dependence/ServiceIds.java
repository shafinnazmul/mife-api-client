package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates.dependence;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class ServiceIds implements Serializable {

    private static final long serialVersionUID = 5588842537686613001L;

    private List<String> serviceId;

}
