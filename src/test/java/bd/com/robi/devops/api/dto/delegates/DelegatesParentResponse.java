package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegates;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.Returnable;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate.Delegate;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import lombok.*;

import java.io.Serializable;
import java.util.List;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DelegatesParentResponse implements Serializable, Returnable<Delegates, ResponseStatus, Dependence> {

    private static final long serialVersionUID = -2735908236210264305L;

    private ErrorResponse errorResponse;

    private DelegatesResponse delegatesResponse;

    private Integer httpResponseStatus;


    public static DelegatesParentResponse empty() {
        return DelegatesParentResponse.builder()
                .delegatesResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasDelegatesResponse() {
        return Objects.nonNull(getDelegatesResponse());
    }

    @Override
    public Delegates target() {
        if (hasDelegatesResponse())
            return delegatesResponse.getDelegates();
        return null;
    }

    @Override
    public ResponseStatus error() {
        if (hasErrorResponse())
            return errorResponse.getResponseStatus();
        return null;
    }

    @Override
    public Dependence dependence() {
        return null;
    }

    public List<Delegate> getDelegateList() {
        return target().getDelegates();
    }
}
