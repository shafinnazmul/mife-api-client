package com.mashfin.licenseissuer.mife.api.resource;

import bd.com.robi.devops.exception.ApiException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates.LicenseAuthDelegatesGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates.LicenseAuthDelegatesParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates.LicenseAuthDelegatesPut;


public interface LicenseAuthDelegatesResource {
    LicenseAuthDelegatesParentResponse getDelegates(LicenseAuthDelegatesGetParams licenseAuthDelegatesGetParams, String requestId) throws ApiException;

    LicenseAuthDelegatesParentResponse updateDelegates(LicenseAuthDelegatesPut delegatesPutParams, String requestId) throws ApiException;
}
