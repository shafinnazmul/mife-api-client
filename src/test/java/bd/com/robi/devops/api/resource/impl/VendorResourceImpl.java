package com.mashfin.licenseissuer.mife.api.resource.impl;


import bd.com.robi.devops.constant.LicenseIssuerResponseConstant;
import bd.com.robi.devops.exception.ApiException;
import bd.com.robi.devops.exception.SystemException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.VendorCreateRequest;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.VendorGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.VendorParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.VendorResponse;
import com.mashfin.licenseissuer.mife.api.resource.VendorResource;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import bd.com.robi.devops.support.ThrowableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static com.mashfin.licenseissuer.mife.api.support.JAXRSClientUtil.prepareResource;


@Slf4j
public class VendorResourceImpl implements VendorResource {

    @Bean
    private ThrowableException throwable() {
        return new ThrowableException();
    }

    private ApiConnectionProvider authProvider;

    public VendorResourceImpl(ApiConnectionProvider apiConnectionProvider) {
        this.authProvider = apiConnectionProvider;
    }

    private String serviceEndPoint(String providerUri) {
        return authProvider.serviceUri() + providerUri;
    }

    private String getURI() {
        return serviceEndPoint("mife.operator.vendor.action");
    }

    @Override
    public VendorParentResponse getVendor(VendorGetParams vendorGetParams, String requestId) throws ApiException {
        try {
            VendorResponse response = prepareResource(getURI(), VendorResourceMapping.class, authProvider)
                    .getVendor(vendorGetParams);
            return VendorParentResponse
                    .builder()
                    .vendorResponse(response)
                    .httpResponseStatus(200)
                    .build();
        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return VendorParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }

    @Override
    public VendorParentResponse postVendor(VendorCreateRequest vendorCreateRequest, String requestId) throws ApiException {
        try {
            VendorResourceMapping vendorResourceMapping = prepareResource(getURI(),
                    VendorResourceMapping.class, authProvider);
            VendorResponse vendorResponse = vendorResourceMapping.postVendor(vendorCreateRequest);
            return VendorParentResponse
                    .builder()
                    .vendorResponse(vendorResponse)
                    .httpResponseStatus(200)
                    .build();
        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return VendorParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }

    public interface VendorResourceMapping {
        @GET
        VendorResponse getVendor(@BeanParam VendorGetParams vendorGetParams)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @POST
        VendorResponse postVendor(@NotNull VendorCreateRequest vendorCreateRequest)
                throws ProcessingException, NotAuthorizedException, NullPointerException;
    }
}
