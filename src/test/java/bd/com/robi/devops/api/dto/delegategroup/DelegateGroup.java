package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup;

import com.mashfin.licenseissuer.mife.api.entity.DateAdapter;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Getter
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "MDGBuilder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class DelegateGroup implements Serializable {

    private static final long serialVersionUID = 5326501525144434461L;

    private String moderatordelegategroupId;

    private String moderatordelegategroupName;

    private String aliasName;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date createTime;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date updateTime;


    @Builder(builderClassName = "Builder", toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class DelegateGroupWrapper implements Serializable {

        private static final long serialVersionUID = -2118852599418537024L;

        @XmlElement(name = "moderatordelegategroup")
        private List<DelegateGroup> items;
    }

}
