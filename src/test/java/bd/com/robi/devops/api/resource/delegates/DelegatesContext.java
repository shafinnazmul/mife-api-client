package bd.com.robi.devops.api.resource.delegates;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.DelegatesResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.DelegatesResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"com.mashfin.licenseissuer.mife.api.resource"})
public class DelegatesContext extends BaseContext {

    @Bean
    public DelegatesResource delegatesResource() {
        return new DelegatesResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }
}
