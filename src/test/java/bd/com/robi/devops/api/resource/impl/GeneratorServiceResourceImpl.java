package com.mashfin.licenseissuer.mife.api.resource.impl;

import com.mashfin.licenseissuer.mife.api.support.Preconditions;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService.GeneratorServiceParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService.GeneratorServiceRequestBody;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService.GeneratorServiceResponse;
import com.mashfin.licenseissuer.mife.api.resource.GeneratorServiceResource;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import com.mashfin.licenseissuer.mife.api.support.JAXRSClientUtil;
import lombok.extern.slf4j.Slf4j;

import javax.el.MethodNotFoundException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Slf4j
public class GeneratorServiceResourceImpl implements GeneratorServiceResource {
    private static final String SERVICE_URI = "/moderator/generator/service";

    private ApiConnectionProvider authProvider;

    public GeneratorServiceResourceImpl(ApiConnectionProvider apiConnectionProvider) {

        this.authProvider = apiConnectionProvider;
    }


    private String serviceEndPoint(String providerUri) {

        return authProvider.serviceUri() + providerUri;
    }

    @Override
    public GeneratorServiceParentResponse postGeneratorService(GeneratorServiceRequestBody generatorServiceRequestBody) {
        try {
            Preconditions.checkRequired(generatorServiceRequestBody, "generatorServicePostBody required!");
            try {
                GeneratorServiceResponse response = JAXRSClientUtil.prepareResource(serviceEndPoint(SERVICE_URI),
                        GeneratorServiceResourceImpl.GeneratorServiceResourceMapping.class,
                        authProvider).postGeneratorService(generatorServiceRequestBody);
                return GeneratorServiceParentResponse
                        .builder()
                        .generatorServiceResponse(response)
                        .httpResponseStatus(200)
                        .build();
            } catch (NotAuthorizedException | BadRequestException ex) {
                Response response = ex.getResponse();
                return GeneratorServiceParentResponse.builder()
                        .errorResponse(response.readEntity(ErrorResponse.class))
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();

            } catch (InternalServerErrorException | NotAllowedException | NotSupportedException | NotFoundException ex) {
                return GeneratorServiceParentResponse.builder()
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();

            } catch (MethodNotFoundException | ProcessingException ex) {

                return GeneratorServiceParentResponse.builder().build();

            }

        } catch (Exception e) {
            log.error("opps! what's happened ", e);
        }
        return null;
    }

    @Override
    public GeneratorServiceParentResponse putGeneratorService(GeneratorServiceRequestBody generatorServiceRequestBody) {
        try {
            Preconditions.checkRequired(generatorServiceRequestBody, "generatorServicePostBody required!");
            try {
                GeneratorServiceResponse response = JAXRSClientUtil.prepareResource(serviceEndPoint(SERVICE_URI),
                        GeneratorServiceResourceImpl.GeneratorServiceResourceMapping.class,
                        authProvider).putGeneratorService(generatorServiceRequestBody);
                return GeneratorServiceParentResponse
                        .builder()
                        .generatorServiceResponse(response)
                        .httpResponseStatus(200)
                        .build();
            } catch (NotAuthorizedException | BadRequestException ex) {
                Response response = ex.getResponse();
                return GeneratorServiceParentResponse.builder()
                        .errorResponse(response.readEntity(ErrorResponse.class))
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();

            } catch (InternalServerErrorException | NotAllowedException | NotSupportedException | NotFoundException ex) {
                return GeneratorServiceParentResponse.builder()
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();

            } catch (MethodNotFoundException | ProcessingException ex) {

                return GeneratorServiceParentResponse.builder().build();

            }

        } catch (Exception e) {
            log.error("opps! what's happened ", e);
        }
        return null;
    }

    public interface GeneratorServiceResourceMapping {
        @POST
        GeneratorServiceResponse postGeneratorService(@NotNull GeneratorServiceRequestBody generatorServiceRequestBody)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @PUT
        GeneratorServiceResponse putGeneratorService(@NotNull GeneratorServiceRequestBody generatorServiceRequestBody)
                throws ProcessingException, NotAuthorizedException, NullPointerException;
    }
}
