package com.mashfin.licenseissuer.mife.api.endpoint.dto.license;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

import java.util.Objects;
import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(FIELD)
@Getter
@Setter
public class LicenseDependence implements Serializable {
    private static final long serialVersionUID = 5408422699977638879L;

    @XmlElement(name = "licenseIds")
    private LicenseIds licenseIds;

    @XmlElement(name = "licensetemplateIds")
    private LicenseTemplateIds licensetemplateIds;

    @XmlElement(name = "serviceIds")
    private ServiceIds serviceIds;

    @XmlElement(name = "vendorIds")
    private VendorIds vendorIds;

    public static LicenseDependence empty() {
        return LicenseDependence.builder()
                .licenseIds(null)
                .licensetemplateIds(null)
                .serviceIds(null)
                .vendorIds(null)
                .build();
    }

    public boolean hasLicenseIds() {
        return Objects.nonNull(this.getLicenseIds());
    }

    public boolean hasLicenseTemplateIds() {
        return Objects.nonNull(this.getLicensetemplateIds());
    }

    public boolean hasServiceIds() {
        return Objects.nonNull(this.getServiceIds());
    }

    public boolean hasVendorIds() {
        return Objects.nonNull(this.getVendorIds());
    }

    @XmlAccessorType(FIELD)
    @Getter
    @Setter
    private static class LicenseIds implements Serializable {
        private static final long serialVersionUID = 6579040415480146874L;

        @XmlElement(name = "licenseId")
        private String licenseIds;
    }

    @XmlAccessorType(FIELD)
    @Getter
    @Setter
    private static class LicenseTemplateIds implements Serializable {
        private static final long serialVersionUID = 4711578967593580406L;

        @XmlElement(name = "licensetemplateId")
        private List<String> licensetemplateIds;

    }

    @XmlAccessorType(FIELD)
    @Getter
    @Setter
    private static class ServiceIds implements Serializable {
        private static final long serialVersionUID = 5588842537686613001L;

        @XmlElement(name = "serviceId")
        private List<String> serviceIds;
    }

    @XmlAccessorType(FIELD)
    @Getter
    @Setter
    private static class VendorIds implements Serializable {
        private static final long serialVersionUID = 4663734406854882478L;

        @XmlElement(name = "vendorId")
        private List<String> vendorIds;
    }
}
