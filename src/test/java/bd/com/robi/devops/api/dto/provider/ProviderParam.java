package com.mashfin.licenseissuer.mife.api.endpoint.dto.provider;

import lombok.Builder;
import lombok.Getter;

import javax.ws.rs.QueryParam;
import java.io.Serializable;



@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
public class ProviderParam implements Serializable {
    private static final long serialVersionUID = 5735731646102410844L;
    private String externalSystemId;
    private String aliasName;

    public ProviderParam(String externalSystemId, String aliasName) {
        this.externalSystemId = externalSystemId;
        this.aliasName = aliasName;
    }

    @QueryParam("externalSystemId")
    public void setExternalSystemId(String externalSystemId) {
        this.externalSystemId = externalSystemId;
    }

    @QueryParam("aliasName")
    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }


}
