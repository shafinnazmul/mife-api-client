package com.mashfin.licenseissuer.mife.api.security;



public interface ProxyProvider {

    String proxyHost();
    int proxyPort();
}
