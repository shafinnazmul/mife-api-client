package bd.com.robi.devops.api.resource;


import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import com.mashfin.licenseissuer.mife.api.security.AuthInfo;
import org.springframework.context.annotation.Bean;



public class BaseContext {

//    @Bean
//    public ApplicationPropertyResource resource() {
//        return ApplicationPropertyResource.instance();
//    }
    
    public ApiConnectionProvider apiConnectionProvider(RequestorType requestorType) {
        return new ApiConnectionProvider() {
            @Override
            public String proxyHost() {
                try {
                    return ("mife.proxy.host");
                } catch (Exception e) {
                    System.out.println("properties file can not be read.");
                }
                return null;
            }

            @Override
            public int proxyPort() {
                try {
                    return Integer.parseInt(("mife.proxy.port"));
                } catch (Exception e) {
                    System.out.println("properties file can not be read.");
                }
                return 0;
            }

            @Override
            public AuthInfo authInfo() {
                return AuthInfo.prepare(requestorType.getName(),
                        ("mife.api.requestor.id"),
                        ("mife.api.requestor.password"),
                        ("mife.api.id"),
                        ("mife.api.password"),
                        ("mife.api.default.content.type")
                );
            }

            @Override
            public String serviceUri() {
                return ("mife.api.base.url");
            }
        };
    }
}
