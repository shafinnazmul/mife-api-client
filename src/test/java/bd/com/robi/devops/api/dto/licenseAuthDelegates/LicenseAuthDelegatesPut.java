package com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "delegatesRequest")
public class LicenseAuthDelegatesPut implements Serializable {

    private static final long serialVersionUID = 8194311315094754806L;

    @XmlElement(name = "licenseRequest")
    private LicenseRequest licenseRequest;

    @XmlElement(name = "delegateIdsRequest")
    private DelegateIdsRequest delegateIdsRequest;
}
