package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates.dependence;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class Dependence implements Serializable {

    private static final long serialVersionUID = 5408422699977638879L;

    @XmlElement(name = "licenseIds")
    private LicenseIds licenseIds;

    @XmlElement(name = "licensetemplateIds")
    private LicenseTemplateIds licensetemplateIds;

    @XmlElement(name = "serviceIds")
    private ServiceIds serviceIds;

    @XmlElement(name = "vendorIds")
    private VendorIds vendorIds;


    public static Dependence empty() {
        return Dependence.builder()
                .licenseIds(null)
                .licensetemplateIds(null)
                .serviceIds(null)
                .vendorIds(null)
                .build();
    }

    public boolean hasLicenseIds() {
        return Objects.nonNull(this.getLicenseIds());
    }

    public boolean hasLicenseTemplateIds() {
        return Objects.nonNull(this.getLicensetemplateIds());
    }

    public boolean hasServiceIds() {
        return Objects.nonNull(this.getServiceIds());
    }

    public boolean hasVendorIds() {
        return Objects.nonNull(this.getVendorIds());
    }


    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @XmlAccessorType(XmlAccessType.FIELD)
    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    public static class DependenceWrapper implements Serializable {

        private static final long serialVersionUID = -7010447852391193743L;

        @XmlElement(name = "dependence")
        private List<Dependence> dependences;


    }

}
