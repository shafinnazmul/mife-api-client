package bd.com.robi.devops.api.resource.licenseauthdelegates;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.LicenseAuthDelegatesResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.LicenseAuthDelegatesResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"com.mashfin.licenseissuer.mife.api.resource"})
public class LicenseAuthDelegatesContext extends BaseContext {

    @Bean
    public LicenseAuthDelegatesResource delegatesResource() {
        return new LicenseAuthDelegatesResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }
}
