package com.mashfin.licenseissuer.mife.api.resource;

import bd.com.robi.devops.exception.ApiException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond.*;


public interface DelegateAgentCondResource {

    DelegateAgentCondDeleteParentResponse deleteDelegateAgentCond (String agentId, String delegateId);

    DelegateAgentCondParentResponse getDelegateAgentCond(String agentId,String delegateId);

    DelegateAgentCondParentResponse getDelegateAgentCond(DelegateAgentCondGetParams delegateAgentCondGetParams, String requestId) throws ApiException;

    DelegateAgentCondParentResponse postDelegateAgentCond(DelegateAgentCondPostRequest delegateAgentCondPostRequest, String requestId) throws ApiException;

    DelegateAgentCondParentResponse putDelegateAgentCond(DelegateAgentCondPutRequest delegateAgentCondPutRequest, String requestId) throws ApiException;
}
