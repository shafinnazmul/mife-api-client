package com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicensetemplateIds implements Serializable {

    private static final long serialVersionUID = -2136933120932776637L;

    private String[] licensetemplateId;
}
