package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Collections;
import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "moderatordelegategroupsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class DelegateGroupsResponse implements Serializable {

    private static final long serialVersionUID = -3619311629841654457L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "totalCount")
    private Integer totalCount;

    @XmlElement(name = "moderatordelegategroups")
    private DelegateGroup.DelegateGroupWrapper result;


    public static DelegateGroupsResponse empty() {
        return DelegateGroupsResponse.builder()
                .result(DelegateGroup.DelegateGroupWrapper.builder()
                        .items(Collections.EMPTY_LIST)
                        .build())
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(getResult()) && !Objects.isNull(getResult().getItems());
    }
}
