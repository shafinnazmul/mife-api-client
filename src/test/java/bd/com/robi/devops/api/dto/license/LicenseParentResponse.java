package com.mashfin.licenseissuer.mife.api.endpoint.dto.license;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.Returnable;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import lombok.*;

import java.io.Serializable;

import java.util.Objects;

@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LicenseParentResponse implements Serializable,
        Returnable<License, ResponseStatus, Dependence> {
    private static final long serialVersionUID = 6380729401026241337L;

    private ErrorResponse errorResponse;

    private LicenseResponse licenseResponse;

    Integer httpResponseStatus;

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasLicenseResponse() {
        return Objects.nonNull(this.getLicenseResponse());
    }

    @Override
    public License target() {
        if (hasLicenseResponse())
            return licenseResponse.getLicense();
        return null;
    }

    @Override
    public ResponseStatus error() {
        if (hasErrorResponse())
            return errorResponse.getResponseStatus();
        return null;
    }

    @Override
    public Dependence dependence() {
        if (hasLicenseResponse())
            return licenseResponse.getDependence();
        return null;
    }

    public String getLicenseId() {
        if (hasLicenseResponse())
            return target().getLicenseId();
        return null;
    }

    public String getServiceId() {
        if (hasLicenseResponse())
            return dependence().getServiceIds().getServiceIds().get(0);
        return null;
    }
}