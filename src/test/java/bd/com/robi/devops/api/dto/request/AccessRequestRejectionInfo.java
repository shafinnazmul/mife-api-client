package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;


@RequiredArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@XmlAccessorType(XmlAccessType.FIELD)
@Builder
public class AccessRequestRejectionInfo implements Serializable {

    private static final long serialVersionUID = 8440141043207665515L;

    @NotNull
    private String requestId;

    @NotNull
    private String approverAccount;

    @NotNull
    private Integer status;

    @NotNull
    private String requesterEmail;

    private String note;

    @NotNull
    private Integer action;

    @AllArgsConstructor
    @RequiredArgsConstructor
    @Getter
    @Setter
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AccessRequestRejectionWrapper implements Serializable {

        private static final long serialVersionUID = -8229994710038111287L;

        @XmlElement(name = "accessRequest")
        private List<AccessRequestRejectionInfo> items;
    }
}
