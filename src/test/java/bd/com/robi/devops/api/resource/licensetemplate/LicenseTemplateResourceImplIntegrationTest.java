package bd.com.robi.devops.api.resource.licensetemplate;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.LicenseTemplateParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.LicenseTemplatePost;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.LicenseTemplateRequest;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.ServiceRequest;
import com.mashfin.licenseissuer.mife.api.resource.LicenseTemplateResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.UUID;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {LicenseTemplateContext.class})
public class LicenseTemplateResourceImplIntegrationTest {

    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private LicenseTemplateResource licenseTemplateResource;

    @Test
    public void addLicenseTemplatesValidResponseTest() {
        LicenseTemplateParentResponse response = licenseTemplateResource
                .addLicenseTemplate(LicenseTemplatePost.builder()
                        .serviceRequest(ServiceRequest.builder()
                                .serviceId("a0269a30-841a-11e7-acba-0022196ab061").build())
                        .licenseTemplateRequest(LicenseTemplateRequest.builder()
                                .licensetemplateName(UUID.randomUUID().toString())
                                .registableState(1)
                                .reachableState(1)
                                .chargeableState(1)
                                .demoVisible(1)
                                .moderatorApprovalState(1)
                                .vendorApprovalState(1)
                                .providerApprovalState(1)
                                .sourceIpaddress("127.0.0.1/0")
                                .maxQueryPercentage(1)
                                .build())
                        .build()
                );
        Assert.notNull(response.getLicenseTemplateResponse().getLicenseTemplate());
    }

    @Test
    public void addLicenseTemplatesErrorResponseTest() throws Exception {
        LicenseTemplateParentResponse response = licenseTemplateResource
                .addLicenseTemplate(LicenseTemplatePost.builder()
                        .serviceRequest(ServiceRequest.builder().serviceId("absdfcd").build())
                        .licenseTemplateRequest(LicenseTemplateRequest.builder()
                                .licensetemplateName("lskdjflksdjf")
                                .registableState(1)
                                .reachableState(1)
                                .chargeableState(1)
                                .demoVisible(1)
                                .sourceIpaddress("dkjklsdjfjsdf")
                                .maxQueryPercentage(1)
                                .build())
                        .build()
                );
        Assert.notNull(response.getErrorResponse());
    }

}
