package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "licensetemplateCreateRequest")
public class LicenseTemplatePost implements Serializable {

    private static final long serialVersionUID = -1082820032781481433L;

    @XmlElement(name = "serviceRequest")
    private ServiceRequest serviceRequest;

    @XmlElement(name = "licensetemplateRequest")
    private LicenseTemplateRequest licenseTemplateRequest;
}
