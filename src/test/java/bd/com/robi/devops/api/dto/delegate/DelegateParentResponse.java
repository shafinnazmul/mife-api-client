package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.Returnable;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import lombok.*;

import java.io.Serializable;
import java.util.Objects;



@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DelegateParentResponse implements Serializable, Returnable<Delegate, ResponseStatus, Dependence> {

    private static final long serialVersionUID = 6380729401026241337L;

    private ErrorResponse errorResponse;

    private DelegateResponse delegateResponse;

    private Integer httpResponseStatus;


    public static DelegateParentResponse empty() {
        return DelegateParentResponse.builder()
                .delegateResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasDelegateResponse() {
        return Objects.nonNull(getDelegateResponse());
    }

    @Override
    public Delegate target() {
        if (hasDelegateResponse())
            return delegateResponse.getDelegate();
        return null;
    }

    @Override
    public ResponseStatus error() {
        if (hasErrorResponse())
            return errorResponse.getResponseStatus();
        return null;
    }

    @Override
    public Dependence dependence() {
        return null;
    }

    public String getDelegateId() {
        if (hasDelegateResponse())
            return delegateResponse.getDelegate().getDelegateId();
        return null;
    }
}
