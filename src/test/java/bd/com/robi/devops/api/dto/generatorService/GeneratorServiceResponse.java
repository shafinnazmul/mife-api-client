package com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.LicenseTemplate;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.service.Service;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor.Vendor;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "generatorServiceResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class GeneratorServiceResponse implements Serializable {
    private static final long serialVersionUID = 4448329276837114682L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "service")
    private Service service;

    @XmlElement(name = "vendor")
    private Vendor vendor;

    @XmlElement(name = "licensetemplate")
    private LicenseTemplate licenseTemplate;

    @XmlElement(name = "delegateIds")
    private DelegateIdsRequest delegateIds;

    public static GeneratorServiceResponse empty() {
        return GeneratorServiceResponse.builder()
                .service(null)
                .vendor(null)
                .licenseTemplate(null)
                .delegateIds(null)
                .responseStatus(null)
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(getService());
    }
}
