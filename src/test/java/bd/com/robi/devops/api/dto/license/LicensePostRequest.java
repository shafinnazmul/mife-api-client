package com.mashfin.licenseissuer.mife.api.endpoint.dto.license;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@Getter
@Setter
@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "licenseCreateRequest")
@XmlAccessorType(FIELD)
public class LicensePostRequest implements Serializable {
    private static final long serialVersionUID = -4292804956861338329L;

    private String licensetemplateId;

    private int externalSystemId;

    private String externalProviderId;

    private String prefix;

    private int issueLimit;
}
