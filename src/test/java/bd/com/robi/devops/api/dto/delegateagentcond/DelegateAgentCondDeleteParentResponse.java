package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import lombok.*;

import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DelegateAgentCondDeleteParentResponse implements Serializable {

    private static final long serialVersionUID = 6380729401026241337L;

    private ErrorResponse errorResponse;

    private DelegateAgentCondDeleteResponse delegateAgentCondDeleteResponse;

    private Integer httpResponseStatus;


    public static DelegateAgentCondDeleteParentResponse empty() {
        return DelegateAgentCondDeleteParentResponse.builder()
                .delegateAgentCondDeleteResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasDelegateAgentCondDeleteResponse() {
        return Objects.nonNull(getDelegateAgentCondDeleteResponse());
    }
}
