package com.mashfin.licenseissuer.mife.api.endpoint.dto.license;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import static javax.xml.bind.annotation.XmlAccessType.FIELD;

@Getter
@Setter
@lombok.Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "licenseRequest")
@XmlAccessorType(FIELD)
public class LicensesPutRequest {
    License license;

    @Getter
    @Setter
    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @XmlRootElement(name = "license")
    @XmlAccessorType(FIELD)
    public static class License {
        String licenseId;
        int moderatorApprovalState;
        int vendorApprovalState;
        int vendorApprovalStateLockFlag;
        int providerApprovalState;
        int providerApprovalStateLockFlag;
        String clientEnvId;
    }
}