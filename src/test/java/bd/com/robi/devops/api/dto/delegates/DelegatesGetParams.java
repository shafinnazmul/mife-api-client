package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegates;

import lombok.Builder;
import lombok.Getter;

import javax.ws.rs.QueryParam;
import java.io.Serializable;



@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
public class DelegatesGetParams implements Serializable {

    private static final long serialVersionUID = -165411560929995771L;

    private String sortKey;
    private String sortOrder;
    private Integer offset;
    private Integer hits;
    private String delegateGroupId;

    public DelegatesGetParams(String sortKey, String sortOrder,
                              Integer offset, Integer hits,
                              String delegateGroupId) {
        this.sortKey = sortKey;
        this.sortOrder = sortOrder;
        this.offset = offset;
        this.hits = hits;
        this.delegateGroupId = delegateGroupId;
    }

    @QueryParam("sortKey")
    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

    @QueryParam("sortOrder")
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @QueryParam("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @QueryParam("hits")
    public void setHits(Integer hits) {
        this.hits = hits;
    }

    @QueryParam("delegategroupId")
    public void setDelegateGroupId(String delegateGroupId) {
        this.delegateGroupId = delegateGroupId;
    }
}
