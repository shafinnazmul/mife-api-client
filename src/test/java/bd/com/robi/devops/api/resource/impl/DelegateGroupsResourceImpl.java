package com.mashfin.licenseissuer.mife.api.resource.impl;


import bd.com.robi.devops.constant.LicenseIssuerResponseConstant;
import bd.com.robi.devops.exception.ApiException;
import bd.com.robi.devops.exception.SystemException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup.DelegateGroupsGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup.DelegateGroupsParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup.DelegateGroupsResponse;
import com.mashfin.licenseissuer.mife.api.resource.DelegateGroupsResource;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import com.mashfin.licenseissuer.mife.api.support.JAXRSClientUtil;
import bd.com.robi.devops.support.ThrowableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Slf4j
public class DelegateGroupsResourceImpl implements DelegateGroupsResource {


    @Bean
    private ThrowableException throwable() {
        return new ThrowableException();
    }

    private ApiConnectionProvider authProvider;

    public DelegateGroupsResourceImpl(ApiConnectionProvider apiConnectionProvider) {

        this.authProvider = apiConnectionProvider;
    }

    private String serviceEndPoint(String providerUri) {

        return authProvider.serviceUri() + providerUri;
    }

    private String getURI() {
        return serviceEndPoint(("mife.operator.moderatordelegategroups.action"));
    }

    @Override
    public DelegateGroupsParentResponse getDelegateGroups(DelegateGroupsGetParams delegateGetParams, String requestId) throws ApiException {
        try {
            DelegateGroupsResponse response = JAXRSClientUtil.prepareResource(getURI(),
                    DelegateResourceMapping.class,
                    authProvider).getDelegateGroups(delegateGetParams);
            return DelegateGroupsParentResponse.builder().delegateGroupsResponse(response).httpResponseStatus(200).build();
        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return DelegateGroupsParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }


    public interface DelegateResourceMapping {
        @GET
        DelegateGroupsResponse getDelegateGroups(@BeanParam DelegateGroupsGetParams param);
    }

}
