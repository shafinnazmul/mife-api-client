package com.mashfin.licenseissuer.mife.api.resource;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService.GeneratorServiceParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService.GeneratorServiceRequestBody;


public interface GeneratorServiceResource {
    GeneratorServiceParentResponse postGeneratorService(GeneratorServiceRequestBody generatorServiceRequestBody);
    GeneratorServiceParentResponse putGeneratorService(GeneratorServiceRequestBody generatorServiceRequestBody);
}
