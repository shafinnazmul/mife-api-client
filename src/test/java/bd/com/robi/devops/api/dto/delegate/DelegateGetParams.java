package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.io.Serializable;



@Getter
@Builder(builderClassName = "Builder", toBuilder = true)
public class DelegateGetParams implements Serializable {

    private static final long serialVersionUID = -4292804956861338329L;

    private String delegateId;

    public DelegateGetParams(String delegateId) {
        this.delegateId = delegateId;
    }

    @QueryParam("delegateId")
    public void setDelegateId(String delegateId) {
        this.delegateId = delegateId;
    }

}
