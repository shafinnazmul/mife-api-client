package bd.com.robi.devops.api.resource.service;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.ServiceResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.ServiceResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"bd.com.robi.devops.api.resource"})
public class ServiceContext extends BaseContext {
    @Bean
    public ServiceResource serviceRepository() {
        return new ServiceResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }
}
