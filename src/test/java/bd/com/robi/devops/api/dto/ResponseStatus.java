package com.mashfin.licenseissuer.mife.api.endpoint.dto;

import com.mashfin.licenseissuer.mife.api.entity.MiliSecondDateAdapter;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class ResponseStatus implements Serializable {

    private static final long serialVersionUID = -720591393202777871L;

    private String requestId;

    @XmlJavaTypeAdapter(MiliSecondDateAdapter.class)
    private Date requestTime;

    private String resultCode;

    private String errorCode;

    private String errorName;

    private String errorReason;

    private String message;
}
