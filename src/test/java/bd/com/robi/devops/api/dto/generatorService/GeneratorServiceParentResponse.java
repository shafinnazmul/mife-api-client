package com.mashfin.licenseissuer.mife.api.endpoint.dto.generatorService;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.service.ModeratorResponse;
import lombok.*;

import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GeneratorServiceParentResponse extends ModeratorResponse implements Serializable {
    private static final long serialVersionUID = 2912612079882468419L;
    protected ErrorResponse errorResponse;

    protected Integer httpResponseStatus;

    private GeneratorServiceResponse generatorServiceResponse;

    public static GeneratorServiceParentResponse empty() {
        return GeneratorServiceParentResponse.builder()
                .generatorServiceResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasServiceResponse() {
        return Objects.nonNull(getGeneratorServiceResponse());
    }
}
