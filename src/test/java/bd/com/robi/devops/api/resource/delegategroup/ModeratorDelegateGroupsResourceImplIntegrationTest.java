package bd.com.robi.devops.api.resource.delegategroup;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup.DelegateGroupsGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup.DelegateGroupsParentResponse;
import com.mashfin.licenseissuer.mife.api.entity.SortOrder;
import com.mashfin.licenseissuer.mife.api.resource.DelegateGroupsResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DelegateGroupContext.class})
public class ModeratorDelegateGroupsResourceImplIntegrationTest {

    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private DelegateGroupsResource delegateResource;

    @Test
    public void getModeratorDelegateGroupsInvalidResponseTest() throws Exception {
        DelegateGroupsParentResponse delegateParentResponse = delegateResource.getDelegateGroups(
                DelegateGroupsGetParams
                        .builder()
                        .build(), "requestId");
        Assert.notNull(delegateParentResponse.getErrorResponse());
    }

    @Test
    public void getModeratorDelegateGroupsValidResponseTest() throws Exception {
        DelegateGroupsParentResponse delegateParentResponse = delegateResource.getDelegateGroups(
                DelegateGroupsGetParams
                .builder()
                .hits(30)
                .offset(0)
                .sortKey("moderatordelegategroup.aliasName")
                .sortOrder(SortOrder.ASCENDING.getValue())
                .build(), "requestId");
        Assert.notNull(delegateParentResponse.getDelegateGroupsResponse().getResult().getItems());
    }


}
