package com.mashfin.licenseissuer.mife.api.endpoint.dto.service;

import com.mashfin.licenseissuer.mife.api.entity.DateTimeAdapter;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class Service implements Serializable {

    private static final long serialVersionUID = -8848098724022852386L;

    private String serviceId;

    private String serviceName;

    private String aliasName;

    private Integer registableState;

    private Integer reachableState;

    private Integer chargeableState;

    private Integer demoVisible;

    private String sourceIpaddress;

    private String contactMail;

    private Integer maxQueryPercentage;

    private String parentId;

    private String description;

    private String descriptionUrl;

    private Integer moderatorApprovalState;

    private Integer vendorApprovalState;

    private Integer providerApprovalState;

    private String secret;

    private Integer noExpirationFlag;

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private Date createTime;

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private Date updateTime;
}
