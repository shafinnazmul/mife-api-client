package com.mashfin.licenseissuer.mife.api.endpoint.dto.provider;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class Provider implements Serializable {

    private static final long serialVersionUID = -2454683399931946447L;
    private String externalSystemId;

    private String externalProviderId;

    private String aliasName;

}
