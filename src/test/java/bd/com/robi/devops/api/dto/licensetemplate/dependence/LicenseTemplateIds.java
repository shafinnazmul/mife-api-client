package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.dependence;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicenseTemplateIds implements Serializable {

    private static final long serialVersionUID = 4546904924828161960L;

    private List<String> licensetemplateId;

}
