package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.Returnable;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import lombok.*;

import java.io.Serializable;
import java.util.List;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DelegateGroupsParentResponse implements Serializable, Returnable<List<DelegateGroup>, ResponseStatus, Dependence> {

    private static final long serialVersionUID = 6380729401026241337L;

    private ErrorResponse errorResponse;

    private DelegateGroupsResponse delegateGroupsResponse;

    private Integer httpResponseStatus;


    public static DelegateGroupsParentResponse empty() {
        return DelegateGroupsParentResponse.builder()
                .delegateGroupsResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasDelegateGroupsResponse() {
        return Objects.nonNull(getDelegateGroupsResponse());
    }

    @Override
    public List<DelegateGroup> target() {
        if (hasDelegateGroupsResponse())
            return delegateGroupsResponse.getResult().getItems();
        return null;
    }

    @Override
    public ResponseStatus error() {
        if (hasErrorResponse())
            return errorResponse.getResponseStatus();
        return null;
    }

    @Override
    public Dependence dependence() {
        return null;
    }

    public Integer totalCount() {
        if (hasDelegateGroupsResponse())
            return delegateGroupsResponse.getTotalCount();
        return null;
    }
}
