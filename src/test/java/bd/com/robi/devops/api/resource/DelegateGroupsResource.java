package com.mashfin.licenseissuer.mife.api.resource;

import bd.com.robi.devops.exception.ApiException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup.DelegateGroupsGetParams;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegategroup.DelegateGroupsParentResponse;


public interface DelegateGroupsResource {
    DelegateGroupsParentResponse getDelegateGroups(DelegateGroupsGetParams delegateGroupsGetParams, String requestId)
            throws ApiException;
}
