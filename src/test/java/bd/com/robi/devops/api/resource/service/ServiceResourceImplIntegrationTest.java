package bd.com.robi.devops.api.resource.service;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.service.*;
import com.mashfin.licenseissuer.mife.api.resource.ServiceResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.UUID;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceContext.class})
public class ServiceResourceImplIntegrationTest {
    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private ServiceResource serviceResource;

    @Test
    public void getServiceTest() throws Exception {
        ServiceGetParentResponse serviceGetParentResponse = serviceResource.getService(
                ServiceGetParams.builder()
                        .serviceId("d384fa90-2e7c-11e8-af75-425732242c5c")
                        .build(), "requestId"
        );
        Assert.notNull(serviceGetParentResponse.getServiceResponse());
    }

    @Test
    public void postServiceTest() throws Exception {
        ServicePostParentResponse servicePostParentResponse = serviceResource.postService(
                ServicePostBody.builder()
                        .serviceRequest(ServiceRequest.builder()
                                .serviceName(UUID.randomUUID().toString())
                                .aliasName("test")
                                .registableState(1)
                                .reachableState(1)
                                .chargeableState(1)
                                .demoVisible(0)
                                .sourceIpaddress("")
                                .maxQueryPercentage(100)
                                .contactMail("marie@gmail.com")
                                .description("Project for service test")
                                .descriptionUrl("")
                                .moderatorApprovalState(2)
                                .vendorApprovalState(2)
                                .providerApprovalState(2)
                                .prefix("LI")
                                .build())
                        .vendorRequest(VendorRequest.builder()
                                .vendorId("cd267340-3f67-11e7-9cda-b8ac6f151a7d")
                                .build())
                        .build());
        Assert.notNull(servicePostParentResponse.getServiceCreateResponse());
    }

    @Test
    public void putServiceTest() throws Exception {
        ServicePutParentResponse servicePutParentResponse = serviceResource.putService(
                ServicePutBody.builder().servicePutRequest(
                        ServicePutRequest.builder()
                                .serviceId("ab6d64c0-45c0-11e7-9cda-b8ac6f151a7d")
                                .serviceName("1c3b6926-3bb8-47d3-888a-ffcaf7ef9ef6")
                                .aliasName("5995.106730726399")
                                .registableState(1)
                                .reachableState(1)
                                .chargeableState(1)
                                .demoVisible(1)
                                .sourceIpaddress("")
                                .contactMail("marie1@gmail.com")
                                .maxQueryPercentage(100)
                                .description("Project for service 5995.106730726399")
                                .descriptionUrl("")
                                .moderatorApprovalState(2)
                                .vendorApprovalState(2)
                                .providerApprovalState(2)
                                .build()).build(), "requestId");
        Assert.notNull(servicePutParentResponse.getServiceUpdateResponse());
    }
}
