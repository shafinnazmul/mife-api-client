package com.mashfin.licenseissuer.mife.api.resource.impl;

import com.mashfin.licenseissuer.mife.api.support.Preconditions;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.provider.ProviderParam;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.provider.ProviderParentResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.provider.ProviderResponse;
import com.mashfin.licenseissuer.mife.api.resource.ProviderResource;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import com.mashfin.licenseissuer.mife.api.support.JAXRSClientUtil;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Slf4j
public class ProviderResourceImpl implements ProviderResource {

    private static final String PROVIDER_URI = "/moderator/operator/provider";

    private ApiConnectionProvider authProvider;

    public ProviderResourceImpl(ApiConnectionProvider apiConnectionProvider) {
        this.authProvider = apiConnectionProvider;
    }


    @Override
    public ProviderParentResponse getProvider(@NonNull ProviderParam providerParam) {
        try {
            Preconditions.checkRequired(providerParam, "ProviderParam required!");
            try {
                ProviderResponse response = JAXRSClientUtil.prepareResource(serviceEndPoint(PROVIDER_URI),
                        ProviderResourceMapping.class,
                        authProvider).getProvider(providerParam);
                return ProviderParentResponse.builder().providerResponse(response).build();
            } catch (WebApplicationException ex) {
                Response response = ex.getResponse();
                String message = ex.getMessage();
                return ProviderParentResponse.builder().errorResponse(response.readEntity(ErrorResponse.class)).build();

            }

        } catch (NotAllowedException e) {
            log.error("Method Not Allowed :", e);
        } catch (NotSupportedException e) {
            log.error("Unsupported media type :", e);
        } catch (BadRequestException e) {
            log.error("Bad Request :", e);
        } catch (NotAuthorizedException e) {
            log.error("Unable to Authorize::", e);
        } catch (InternalServerErrorException e) {
            log.error("Internal server error ::", e);
        } catch (ProcessingException e) {
            log.error("unable to connect web service : ", e);
        } catch (Exception e) {
            log.error("opps! whats happen ", e);
        }
        return null;
    }

    private String serviceEndPoint(String providerUri) {

        return authProvider.serviceUri() + providerUri;
    }


    public interface ProviderResourceMapping {
        @GET
        ProviderResponse getProvider(@BeanParam ProviderParam param)
                throws ProcessingException, NotAuthorizedException, NullPointerException;
    }

}
