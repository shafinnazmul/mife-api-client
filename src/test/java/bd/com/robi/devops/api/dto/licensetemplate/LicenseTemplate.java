package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate;

import com.mashfin.licenseissuer.mife.api.entity.MiliSecondDateAdapter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicenseTemplate implements Serializable {

    private static final long serialVersionUID = 6377411285740590218L;

    private String licensetemplateId;

    private String licensetemplateName;

    private String aliasName;

    private String sourceIpaddress;

    private String contactMail;

    private String parentId;

    private String description;

    private String descriptionUrl;

    private String downloadUrl;

    private String developperMail;

    private String creationRequestModeratorId;

    private String moderatorDelegateGroupId;

    private Integer registableState;

    private Integer reachableState;

    private Integer chargeableState;

    private Integer demoVisible;

    private Integer maxQueryPercentage;

    private Integer moderatorApprovalState;

    private Integer vendorApprovalState;

    private Integer providerApprovalState;

    private Integer limitDays;

    private Integer noExpirationFlag;

    @XmlJavaTypeAdapter(MiliSecondDateAdapter.class)
    private Date createTime;

    @XmlJavaTypeAdapter(MiliSecondDateAdapter.class)
    private Date updateTime;

}
