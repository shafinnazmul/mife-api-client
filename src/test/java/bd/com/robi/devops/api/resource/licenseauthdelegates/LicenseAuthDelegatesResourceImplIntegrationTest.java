package bd.com.robi.devops.api.resource.licenseauthdelegates;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.licenseAuthDelegates.*;
import com.mashfin.licenseissuer.mife.api.resource.LicenseAuthDelegatesResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {LicenseAuthDelegatesContext.class})
public class LicenseAuthDelegatesResourceImplIntegrationTest {

    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private LicenseAuthDelegatesResource licenseAuthDelegatesResource;

    @Test
    public void getDelegatesErrorResponseTest() throws Exception {
        LicenseAuthDelegatesParentResponse response = licenseAuthDelegatesResource.getDelegates(
                new LicenseAuthDelegatesGetParams(
                        "delegate.delegateId", "ascending", 0, 10, "abcd"), "requestId");
        Assert.notNull(response.getErrorResponse());
    }

    @Test
    public void getDelegatesValidResponseTest() throws Exception {
        LicenseAuthDelegatesParentResponse response = licenseAuthDelegatesResource.getDelegates(
                new LicenseAuthDelegatesGetParams(
                        "delegate.delegateId", "ascending", 0, 10,
                        "1c9965f0-84bd-11e7-9cda-b8ac6f151a7d"), "requestId");
        Assert.notNull(response.getLicenseAuthDelegatesResponse().getLicenseAuthDelegates());
    }

    @Test
    public void updateDelegatesValidResponseTest() throws Exception {

        List<String> delegateIdList = new ArrayList();
//        delegateIdList.add("13d04d30-a477-11e5-b703-0019b9e60f26");
//        delegateIdList.add("3e408710-7312-11e5-bced-0019b9e60f26");
        delegateIdList.add("03e2c3a0-2b14-11e3-892e-0019b9e60f26");

        LicenseAuthDelegatesParentResponse response = licenseAuthDelegatesResource.updateDelegates(
                LicenseAuthDelegatesPut.builder()
                        .delegateIdsRequest(DelegateIdsRequest.builder().delegateIds(delegateIdList).build())
                        .licenseRequest(LicenseRequest.builder().licenseId("b55106b0-6552-11e8-b1b0-425732242c5c").build())
                        .build(), "requestId");
        Assert.notNull(response.getLicenseAuthDelegatesResponse().getLicenseAuthDelegates());
    }

    @Test
    public void updateDelegatesInvalidResponseTest() throws Exception {
        LicenseAuthDelegatesParentResponse response = licenseAuthDelegatesResource.updateDelegates(
                LicenseAuthDelegatesPut.builder()
                        .delegateIdsRequest(DelegateIdsRequest.builder().build())
                        .licenseRequest(LicenseRequest.builder().licenseId("cd1baed").build())
                        .build(), "requestId");
        Assert.notNull(response.getErrorResponse());
    }
}
