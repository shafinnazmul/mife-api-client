package bd.com.robi.devops.api.resource.mife.operator.license;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.license.*;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.license.LicensesPutRequest.License;
import com.mashfin.licenseissuer.mife.api.resource.LicenseResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import static com.mashfin.licenseissuer.mife.api.endpoint.dto.license.LicensesPutRequest.builder;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ModeratorOperatorLicenseContext.class})
public class LicenseResourceIntegrationTest {

    private final static String VALID_LICENSE_ID = "b55106b0-6552-11e8-b1b0-425732242c5c";
    private final static String INVALID_LICENSE_ID = "invalidLicenseId";

    private final static String LICENSE_TEMPLATE_ID = "91af4540-62f2-11e7-9cda-b8ac6f151a7d";
    private final static int EXTERNAL_SYSTEM_ID = 5;
    private final static String EXTERNAL_PROVIDER_ID = "mashfinInternal";
    private final static int ISSUE_LIMIT = 200;
    private final static String PREFIX = "LI";

    private final static String SORT_KEY = "license.licenseId";
    private final static String SORT_KEY2 = "license.createTime";
    private final static String SORT_ORDER = "descending";
    private final static String SORT_ORDER2 = "descending";
    private final static int HITS = 30;
    private final static int OFFSET = 0;

    private final static String clientEnvId = "undefined";
    private final static int moderatorApprovalState = 1; //0 == undefined, 1 == waiting, 2 == approved, 3 == rejected
    private final static int providerApprovalState = 1;
    private final static int vendorApprovalState = 1;
    private final static int providerApprovalStateLockFlag = 1;
    private final static int vendorApprovalStateLockFlag = 1;

    private final static String invalidSortKey = "invalidSortKey";
    private final static String invalidSortKey2 = "invalidSortKey2";
    private final static String invalidSortOrder = "invalidSortOrder";
    private final static String invalidSortOrder2 = "invalidSortOrder2";

    private final static String invalidLicenseTemplateId = "invalidLicenseTemplateId";
    private final static String invalidExternalProviderId = "invalidExternalProviderId";
    private final static String invalidPrefix = "invalidPrefix";
    private final static int invalidExternalSystemId = 0;
    private final static int invalidIssueLimit = 0;

    @Inject
    private LicenseResource resource;

    @Test
    public void getLicenseTest() throws Exception {
        LicenseParentResponse response = resource.getLicense(VALID_LICENSE_ID, "requestId");
        assertNotNull(response.getLicenseResponse().getLicense().getLicenseId());
        assertNotNull(response.getLicenseResponse().getDependence());
        assertTrue(response.getLicenseResponse().getResponseStatus().getResultCode().equals("OK"));
    }

    @Test
    public void getLicenseErrorResponseTest() throws Exception {
        LicenseParentResponse response = resource.getLicense(INVALID_LICENSE_ID, "requestId");
        assertTrue(response.hasErrorResponse());
    }

    @Test
    public void getLicensesTest() throws Exception {
        LicensesGetParam params = LicensesGetParam.builder()
                .hits(HITS)
                .offset(OFFSET)
                .sortKey(SORT_KEY)
                .sortKey2(SORT_KEY2)
                .sortOrder(SORT_ORDER)
                .sortOrder2(SORT_ORDER2)
                .build();

        LicensesParentResponse response = resource.getLicenses(params, "requestId");
        assertNotNull(response.getLicensesResponse().getLicenses());
        assertNotNull(response.getLicensesResponse().getDependences().getLicenseDependences());
        assertTrue(response.getLicensesResponse().getResponseStatus().getResultCode().equals("OK"));
    }

    @Test
    public void getLicensesErrorResponseTest() throws Exception {
        LicensesGetParam params = LicensesGetParam.builder()
                .hits(HITS)
                .offset(OFFSET)
                .sortKey(invalidSortKey)
                .sortKey2(invalidSortKey2)
                .sortOrder(invalidSortOrder)
                .sortOrder2(invalidSortOrder2)
                .build();

        LicensesParentResponse response = resource.getLicenses(params, "requestId");
        assertTrue(response.hasErrorResponse());
    }

    @Test
    public void addLicenseTest() throws Exception {
        LicensePostRequest params = LicensePostRequest
                .builder()
                .licensetemplateId(LICENSE_TEMPLATE_ID)
                .externalSystemId(EXTERNAL_SYSTEM_ID)
                .externalProviderId(EXTERNAL_PROVIDER_ID)
                .prefix(PREFIX)
                .issueLimit(ISSUE_LIMIT)
                .build();
        LicenseParentResponse response = resource.addLicense(params, "requestId");
        assertNotNull(response.getLicenseResponse().getLicense().getLicenseId());
        assertTrue(response.getLicenseResponse().getResponseStatus().getResultCode().equals("OK"));
    }

    @Test
    public void addLicenseErrorResponseTest() throws Exception {
        LicensePostRequest params = LicensePostRequest
                .builder()
                .licensetemplateId(invalidLicenseTemplateId)
                .externalSystemId(invalidExternalSystemId)
                .externalProviderId(invalidExternalProviderId)
                .prefix(invalidPrefix)
                .issueLimit(invalidIssueLimit)
                .build();
        LicenseParentResponse response = resource.addLicense(params, "requestId");
        assertTrue(response.hasErrorResponse());
    }

    @Test
    public void updateLicenseTest() throws Exception {
        LicensesPutRequest requestBody = builder()
                .license(License.builder()
                        .clientEnvId(clientEnvId)
                        .licenseId(VALID_LICENSE_ID)
                        .moderatorApprovalState(moderatorApprovalState)
                        .providerApprovalState(providerApprovalState)
                        .vendorApprovalState(vendorApprovalState)
                        .providerApprovalStateLockFlag(providerApprovalStateLockFlag)
                        .vendorApprovalStateLockFlag(vendorApprovalStateLockFlag)
                        .build())
                .build();

        LicenseParentResponse response = resource.updateLicense(requestBody, "requestId");
        assertTrue(response.getLicenseResponse().getResponseStatus().getResultCode().equals("OK"));
        assertNotNull(response.getLicenseResponse().getLicense().getLicenseId());
        assertNotNull(response.getLicenseResponse().getDependence().getLicenseIds());
    }

    @Test
    public void updateLicenseErrorResponseTest() throws Exception {
        LicensesPutRequest requestBody = builder()
                .license(License.builder()
                        .clientEnvId(clientEnvId)
                        .licenseId(INVALID_LICENSE_ID)
                        .moderatorApprovalState(moderatorApprovalState)
                        .providerApprovalState(providerApprovalState)
                        .vendorApprovalState(vendorApprovalState)
                        .providerApprovalStateLockFlag(providerApprovalStateLockFlag)
                        .vendorApprovalStateLockFlag(vendorApprovalStateLockFlag)
                        .build())
                .build();

        LicenseParentResponse response = resource.updateLicense(requestBody, "requestId");
        assertTrue(response.hasErrorResponse());
    }
}
