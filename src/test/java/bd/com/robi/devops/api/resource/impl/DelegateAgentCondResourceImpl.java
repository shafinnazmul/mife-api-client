package com.mashfin.licenseissuer.mife.api.resource.impl;


import com.mashfin.licenseissuer.mife.api.support.Preconditions;
import bd.com.robi.devops.constant.LicenseIssuerResponseConstant;
import bd.com.robi.devops.exception.ApiException;
import bd.com.robi.devops.exception.SystemException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond.*;
import com.mashfin.licenseissuer.mife.api.resource.DelegateAgentCondResource;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import com.mashfin.licenseissuer.mife.api.support.JAXRSClientUtil;
import bd.com.robi.devops.support.ThrowableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import javax.el.MethodNotFoundException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Slf4j
public class DelegateAgentCondResourceImpl implements DelegateAgentCondResource {



    @Bean
    private ThrowableException throwable() {
        return new ThrowableException();
    }

    private ApiConnectionProvider authProvider;

    public DelegateAgentCondResourceImpl(ApiConnectionProvider apiConnectionProvider) {

        this.authProvider = apiConnectionProvider;
    }

    private String serviceEndPoint(String providerUri) {

        return authProvider.serviceUri() + providerUri;
    }

    private String getURI() {
        return serviceEndPoint("mife.operator.delegate.agent.cond.action");
    }

    @Override
    public DelegateAgentCondParentResponse getDelegateAgentCond(String agentId, String delegateId) {
        try {
            Preconditions.checkRequired(agentId, "agendId required!");
            Preconditions.checkRequired(delegateId, "delegateId required!");

            try {
                DelegateAgentCondResponse response = JAXRSClientUtil.prepareResource(getURI(),
                        DelegateAgentCondResourceMapping.class,
                        authProvider).getDelegateAgentCond(new DelegateAgentCondGetParams(agentId, delegateId));
                return DelegateAgentCondParentResponse.builder().delegateAgentCondResponse(response).httpResponseStatus(200).build();
            } catch (NotAuthorizedException | BadRequestException ex) {
                Response response = ex.getResponse();
                return DelegateAgentCondParentResponse.builder()
                        .errorResponse(response.readEntity(ErrorResponse.class))
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();

            } catch (InternalServerErrorException | NotAllowedException | NotSupportedException | NotFoundException ex) {
                return DelegateAgentCondParentResponse.builder()
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();

            } catch (MethodNotFoundException | ProcessingException ex) {

                return DelegateAgentCondParentResponse.builder().build();

            }

        } catch (Exception e) {
            log.error("opps! what's happened ", e);
        }
        return null;
    }

    @Override
    public DelegateAgentCondParentResponse getDelegateAgentCond(
            DelegateAgentCondGetParams delegateAgentCondGetParams, String requestId) throws ApiException {
        Preconditions.checkRequired(delegateAgentCondGetParams.getAgentId(), "agentId required!");
        Preconditions.checkRequired(delegateAgentCondGetParams.getDelegateId(), "delegateId required!");
        try {
            DelegateAgentCondResponse response = JAXRSClientUtil.prepareResource(getURI(),
                    DelegateAgentCondResourceMapping.class,
                    authProvider).getDelegateAgentCond(delegateAgentCondGetParams);
            return DelegateAgentCondParentResponse.builder().delegateAgentCondResponse(response).httpResponseStatus(200).build();
        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return DelegateAgentCondParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }

    @Override
    public DelegateAgentCondParentResponse postDelegateAgentCond(DelegateAgentCondPostRequest delegateAgentCondPostRequest, String requestId) throws ApiException {
        try {
            DelegateAgentCondResponse response = JAXRSClientUtil.prepareResource(getURI(),
                    DelegateAgentCondResourceMapping.class,
                    authProvider).postDelegateAgentCond(delegateAgentCondPostRequest);
            return DelegateAgentCondParentResponse.builder().delegateAgentCondResponse(response).httpResponseStatus(200).build();
        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return DelegateAgentCondParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }

    @Override
    public DelegateAgentCondParentResponse putDelegateAgentCond(DelegateAgentCondPutRequest delegateAgentCondPutRequest, String requestId) throws ApiException {
        try {
            DelegateAgentCondResponse response = JAXRSClientUtil.prepareResource(getURI(),
                    DelegateAgentCondResourceMapping.class,
                    authProvider).putDelegateAgentCond(delegateAgentCondPutRequest);
            return DelegateAgentCondParentResponse.builder().delegateAgentCondResponse(response).httpResponseStatus(200).build();
        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return DelegateAgentCondParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }

    @Override
    public DelegateAgentCondDeleteParentResponse deleteDelegateAgentCond(String agentId, String delegateId) {
        try {
            Preconditions.checkRequired(agentId, "agentId required!");
            Preconditions.checkRequired(delegateId, "delegateId required!");

            try {
                DelegateAgentCondDeleteResponse response = JAXRSClientUtil.prepareResource(
                        getURI(), DelegateAgentCondResourceImpl.DelegateAgentCondResourceMapping.class, authProvider)
                        .deleteDelegateAgentCond(DelegateAgentCondDelete.builder()
                                .agentId(agentId).delegateId(delegateId).build());

                return DelegateAgentCondDeleteParentResponse.builder().delegateAgentCondDeleteResponse(response).httpResponseStatus(200).build();
            } catch (NotAuthorizedException | BadRequestException ex) {
                Response response = ex.getResponse();
                return DelegateAgentCondDeleteParentResponse.builder()
                        .errorResponse(response.readEntity(ErrorResponse.class))
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();
            } catch (InternalServerErrorException | NotAllowedException | NotSupportedException | NotFoundException ex) {
                return DelegateAgentCondDeleteParentResponse.builder()
                        .httpResponseStatus(ex.getResponse().getStatus())
                        .build();
            } catch (NullPointerException e) {
                return DelegateAgentCondDeleteParentResponse.builder().build();
            }
        } catch (Exception e) {
            log.error("opps! what's happened ", e);
        }
        return null;
    }


    public interface DelegateAgentCondResourceMapping {
        @GET
        DelegateAgentCondResponse getDelegateAgentCond(@BeanParam DelegateAgentCondGetParams param)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @POST
        DelegateAgentCondResponse postDelegateAgentCond(@NotNull DelegateAgentCondPostRequest delegateAgentCondPostRequest)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @PUT
        DelegateAgentCondResponse putDelegateAgentCond(@NotNull DelegateAgentCondPutRequest delegateAgentCondPutRequest)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @DELETE
        DelegateAgentCondDeleteResponse deleteDelegateAgentCond(@BeanParam DelegateAgentCondDelete param)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

    }

}
