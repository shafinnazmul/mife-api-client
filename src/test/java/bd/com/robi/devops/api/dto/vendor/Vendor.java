package com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor;

import com.mashfin.licenseissuer.mife.api.entity.DateTimeAdapter;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class Vendor implements Serializable {

    private static final long serialVersionUID = 8811280766545607344L;

    private String vendorId;

    private String vendorName;

    private String aliasName;

    private Integer registableState;

    private Integer reachableState;

    private Integer chargeableState;

    private Integer demoVisible;

    private String sourceIpaddress;

    private String contactMail;

    private Integer maxQueryPercentage;

    private String parentId;

    private String description;

    private String descriptionUrl;

    private Integer moderatorApprovalState;

    private Integer vendorApprovalState;

    private Integer providerApprovalState;

    private Integer noExpirationFlag;

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private Date createTime;

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private Date updateTime;
}
