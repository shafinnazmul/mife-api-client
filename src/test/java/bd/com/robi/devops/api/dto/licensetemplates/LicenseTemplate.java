package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplates;

import com.mashfin.licenseissuer.mife.api.entity.DateAdapter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicenseTemplate implements Serializable {

    private static final long serialVersionUID = -5572474647238781272L;

    private String licensetemplateId;

    private String licensetemplateName;

    private String aliasName;

    private String sourceIpaddress;

    private String contactMail;

    private String parentId;

    private String description;

    private String descriptionUrl;

    private String downloadUrl;

    private String developperMail;

    private String creationRequestModeratorId;

    private String moderatorDelegateGroupId;

    private Integer registableState;

    private Integer reachableState;

    private Integer chargeableState;

    private Integer demoVisible;

    private Integer maxQueryPercentage;

    private Integer moderatorApprovalState;

    private Integer vendorApprovalState;

    private Integer providerApprovalState;

    private Integer limitDays;

    private Integer noExpirationFlag;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date createTime;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date updateTime;


    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @XmlAccessorType(XmlAccessType.FIELD)
    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    public static class LicenseTemplateWrapper implements Serializable {

        private static final long serialVersionUID = 8149845735467885248L;

        @XmlElement(name = "licensetemplate")
        private List<LicenseTemplate> licensetemplates;
    }

}
