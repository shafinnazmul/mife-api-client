package com.mashfin.licenseissuer.mife.api.endpoint.dto.delegateagentcond;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.Returnable;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import lombok.*;

import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DelegateAgentCondParentResponse implements Serializable, Returnable<DelegateAgentCond, ResponseStatus, Dependence> {

    private static final long serialVersionUID = 6380729401026241337L;

    private ErrorResponse errorResponse;

    private DelegateAgentCondResponse delegateAgentCondResponse;

    private Integer httpResponseStatus;


    public static DelegateAgentCondParentResponse empty() {
        return DelegateAgentCondParentResponse.builder()
                .delegateAgentCondResponse(null)
                .build();
    }

    public boolean hasErrorResponse() {
        return Objects.nonNull(getErrorResponse());
    }

    public boolean hasDelegateAgentCondResponse() {
        return Objects.nonNull(getDelegateAgentCondResponse());
    }

    @Override
    public DelegateAgentCond target() {
        if (hasDelegateAgentCondResponse())
            return delegateAgentCondResponse.getDelegateAgentCond();
        return null;
    }

    @Override
    public ResponseStatus error() {
        if (hasErrorResponse())
            return errorResponse.getResponseStatus();
        return null;
    }

    @Override
    public Dependence dependence() {
        return null;
    }
}
