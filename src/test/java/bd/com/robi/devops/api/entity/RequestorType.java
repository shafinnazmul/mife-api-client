package com.mashfin.licenseissuer.mife.api.entity;

import lombok.Getter;

import java.util.stream.Stream;



@Getter
public enum RequestorType {
    PROVIDER("provider"), OPERATOR("operator"), VENDOR("vendor"), MODERATOR("moderator");

    private String name;

    RequestorType(String name) {
        this.name = name;
    }

    public static RequestorType findByName(String name) {
        return Stream.of(RequestorType.values())
                .filter(a -> a.getName().equals(name))
                .findFirst()
                .orElse(null);
    }
}
