package com.mashfin.licenseissuer.mife.api.resource.impl;


import bd.com.robi.devops.constant.LicenseIssuerResponseConstant;
import bd.com.robi.devops.exception.ApiException;
import bd.com.robi.devops.exception.SystemException;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ErrorResponse;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.license.*;
import com.mashfin.licenseissuer.mife.api.resource.LicenseResource;
import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import com.mashfin.licenseissuer.mife.api.support.JAXRSClientUtil;
import bd.com.robi.devops.support.ThrowableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import javax.el.MethodNotFoundException;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static com.mashfin.licenseissuer.mife.api.support.Preconditions.checkRequired;

@Slf4j
public class LicenseResourceImpl implements LicenseResource {

    private static final String OPERATOR_LICENSES_URI = "/moderator/operator/licenses";



    @Bean
    private ThrowableException throwable() {
        return new ThrowableException();
    }

    private ApiConnectionProvider apiConnectionProvider;

    public LicenseResourceImpl(ApiConnectionProvider apiConnectionProvider) {
        this.apiConnectionProvider = apiConnectionProvider;
    }

    private String serviceEndPoint(String providerUri) {
        return apiConnectionProvider.serviceUri() + providerUri;
    }

    private String getURI() {
        return serviceEndPoint(("mife.operator.license.action"));
    }

    @Override
    public LicenseParentResponse getLicense(String licenseId, String requestId) throws ApiException {
        try {
            checkRequired(licenseId, "LicenseId is required!");

            LicenseResponse response = JAXRSClientUtil.prepareResource(
                    getURI(),
                    ModeratorOperatorLicenseResourceMapping.class,
                    apiConnectionProvider).getLicense(licenseId);

            return LicenseParentResponse
                    .builder()
                    .licenseResponse(response)
                    .httpResponseStatus(200)
                    .build();

        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return LicenseParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }

    @Override
    public LicenseParentResponse getLicense(String licenseId, String key, String requestId) throws ApiException {
        try {
            LicenseResponse response = JAXRSClientUtil.prepareResource(
                    getURI(),
                    ModeratorOperatorLicenseResourceMapping.class,
                    apiConnectionProvider).getLicense(licenseId, key);

            return LicenseParentResponse
                    .builder()
                    .licenseResponse(response)
                    .httpResponseStatus(200)
                    .build();

        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return LicenseParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }


    @Override
    public LicensesParentResponse getLicenses(LicensesGetParam param, String requestId) throws ApiException {
        try {
            ModeratorOperatorLicenseResourceMapping resourceMapping = JAXRSClientUtil.prepareResource(
                    serviceEndPoint(OPERATOR_LICENSES_URI),
                    ModeratorOperatorLicenseResourceMapping.class,
                    apiConnectionProvider);

            LicensesResponse response = resourceMapping.getLicenses(param);

            return LicensesParentResponse
                    .builder()
                    .licensesResponse(response)
                    .httpResponseStatus(200)
                    .build();

        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return LicensesParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }

    @Override
    public LicenseParentResponse addLicense(LicensePostRequest param, String requestId) throws ApiException {
        try {
            checkRequired(param, "moderatorOperatorLicenseParam is required!");
            checkRequired(param.getExternalProviderId(), "externalProviderId is required!");
            checkRequired(param.getExternalSystemId(), "ExternalSystemId is required!");
            checkRequired(param.getIssueLimit(), "IssueLimit is required!");
            checkRequired(param.getLicensetemplateId(), "LicensetemplateId is required!");
            checkRequired(param.getPrefix(), "Prefix is required!");

            LicenseResponse response = JAXRSClientUtil.prepareResource(
                    getURI(),
                    ModeratorOperatorLicenseResourceMapping.class,
                    apiConnectionProvider).addLicense(param);

            return LicenseParentResponse
                    .builder()
                    .licenseResponse(response)
                    .httpResponseStatus(200)
                    .build();

        } catch (NotAuthorizedException | BadRequestException ex) {
            Response response = ex.getResponse();
            return LicenseParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(ex.getResponse().getStatus())
                    .build();

        } catch (InternalServerErrorException | NotAllowedException | NotSupportedException | NotFoundException ex) {
            return LicenseParentResponse.builder()
                    .httpResponseStatus(ex.getResponse().getStatus())
                    .build();

        } catch (MethodNotFoundException | ProcessingException ex) {
            return LicenseParentResponse.builder().build();

        } catch (Exception e) {
            log.error("opps! Something went wrong!", e);
        }
        return null;
    }

    @Override
    public LicenseParentResponse updateLicense(LicensesPutRequest param, String requestId) throws ApiException {
        try {
            LicenseResponse response = JAXRSClientUtil.prepareResource(
                    getURI(),
                    ModeratorOperatorLicenseResourceMapping.class,
                    apiConnectionProvider).updateLicense(param);

            return LicenseParentResponse
                    .builder()
                    .licenseResponse(response)
                    .httpResponseStatus(200)
                    .build();

        } catch (BadRequestException e) {
            log.error("requestId:" + requestId + " " + getURI(), e);
            Response response = e.getResponse();
            return LicenseParentResponse.builder()
                    .errorResponse(response.readEntity(ErrorResponse.class))
                    .httpResponseStatus(e.getResponse().getStatus())
                    .build();
        } catch (WebApplicationException e) {
            throwable().throwException(e, requestId);
        } catch (ProcessingException e) {
            log.error("requestId:" + requestId + " " + "Timeout to connect API - " + getURI());
            throw new SystemException(
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_FROM_EXTERNAL_API_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.SEVERE_ERROR_FROM_EXTERNAL_API_RESPONSE_STATUS);
        } catch (Exception e) {
            log.error("requestId:" + requestId + " " + "Could not connect to API - " + getURI());
            throw new SystemException(LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_CODE,
                    LicenseIssuerResponseConstant.INTERNAL_SERVER_ERROR_RESPONSE_STATUS);
        }
        return null;
    }


    public interface ModeratorOperatorLicenseResourceMapping {
        @GET
        LicenseResponse getLicense(@QueryParam("licenseId") String licenseId)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @GET
        LicensesResponse getLicenses(@BeanParam LicensesGetParam param)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @POST
        LicenseResponse addLicense(@NotNull LicensePostRequest param)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @PUT
        LicenseResponse updateLicense(@NotNull LicensesPutRequest param)
                throws ProcessingException, NotAuthorizedException, NullPointerException;

        @GET
        LicenseResponse getLicense(@QueryParam("licenseId") String licenseId, @QueryParam("key") String key)
                throws ProcessingException, NotAuthorizedException, NullPointerException;
    }
}
