package com.mashfin.licenseissuer.mife.api.endpoint.dto.vendor;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency.Dependence;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;



@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "vendorResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class VendorResponse implements Serializable {

    private static final long serialVersionUID = -6471616091348465495L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "vendor")
    private Vendor vendor;

    private Dependence dependence;

    public static VendorResponse empty() {
        return VendorResponse.builder()
                .vendor(null)
                .responseStatus(null)
                .dependence(null)
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(getVendor());
    }
}
