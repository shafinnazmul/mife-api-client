package com.mashfin.licenseissuer.mife.api.support;

import com.mashfin.licenseissuer.mife.api.security.ApiConnectionProvider;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.provider.JAXBElementProvider;
import org.apache.cxf.transports.http.configuration.ProxyServerType;

import java.util.Collections;


public class JAXRSClientUtil {
    public static <T> T prepareResource(String baseAddress, Class<T> cls, ApiConnectionProvider authProvider) {

        T resource = JAXRSClientFactory.create(baseAddress, cls, Collections.singletonList(new JAXBElementProvider()));

        if (authProvider.proxyHost() != null && !authProvider.proxyHost().isEmpty()) {
            WebClient.getConfig(resource).getHttpConduit().getClient().setProxyServer(authProvider.proxyHost());
            WebClient.getConfig(resource).getHttpConduit().getClient().setProxyServerPort(authProvider.proxyPort());
            WebClient.getConfig(resource).getHttpConduit().getClient().setProxyServerType(ProxyServerType.HTTP);
        }

        WebClient.client(resource)
                .header("Authorization", authProvider.authInfo().getAuthorization())
                .header("Requestor", authProvider.authInfo().getRequestor())
                .header("Content-Type", authProvider.authInfo().getContentType());

        return resource;


    }
}
