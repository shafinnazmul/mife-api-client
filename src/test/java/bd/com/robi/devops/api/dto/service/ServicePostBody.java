package com.mashfin.licenseissuer.mife.api.endpoint.dto.service;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Getter
@Setter
@XmlRootElement(name = "serviceCreateRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@Builder(builderClassName = "Builder", toBuilder = true)
public class ServicePostBody implements Serializable {

    private static final long serialVersionUID = 1770855911869506859L;

    private ServiceRequest serviceRequest;

    private VendorRequest vendorRequest;

    public ServicePostBody(){}

    public ServicePostBody(ServiceRequest serviceRequest, VendorRequest vendorRequest) {
        this.serviceRequest = serviceRequest;
        this.vendorRequest = vendorRequest;
    }
}
