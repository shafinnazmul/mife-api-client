package com.mashfin.licenseissuer.mife.api.endpoint.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;


@AllArgsConstructor
@Setter
@Getter
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessRequestDeleteInfo implements Serializable {

    private static final long serialVersionUID = 8325406887058231309L;

    private String requestId;


    @AllArgsConstructor
    @Getter
    @Setter
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class AccessRequestDeleteWrapper implements Serializable {

        private static final long serialVersionUID = -8229994710038111287L;

        @XmlElement(name = "accessRequest")
        private List<AccessRequestDeleteInfo> items;
    }

}
