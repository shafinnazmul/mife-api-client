package com.mashfin.licenseissuer.mife.api.endpoint.dto.dependency;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "dependence")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class Dependence implements Serializable {

    private static final long serialVersionUID = -5394724661992358453L;

    private LicensetemplateIds licensetemplateIds;

    private ServiceIds serviceIds;

    private LicenseIds licenseIds;

    private VendorIds vendorIds;
}
