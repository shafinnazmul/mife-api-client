package bd.com.robi.devops.api.resource.delegateagentcond;

import bd.com.robi.devops.api.resource.BaseContext;
import com.mashfin.licenseissuer.mife.api.entity.RequestorType;
import com.mashfin.licenseissuer.mife.api.resource.DelegateAgentCondResource;
import com.mashfin.licenseissuer.mife.api.resource.impl.DelegateAgentCondResourceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(value = {"bd.com.robi.devops.api.resource"})
public class DelegateAgentCondContext extends BaseContext {

    @Bean
    public DelegateAgentCondResource delegateAgentCondResource() {
        return new DelegateAgentCondResourceImpl(apiConnectionProvider(RequestorType.MODERATOR));
    }

}
