package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate;

import com.mashfin.licenseissuer.mife.api.endpoint.dto.ResponseStatus;
import com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.dependence.Dependence;
import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "licensetemplateResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicenseTemplateResponse implements Serializable {

    private static final long serialVersionUID = 3665588978286661586L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;

    @XmlElement(name = "licensetemplate")
    private LicenseTemplate licenseTemplate;

    @XmlElement(name = "dependence")
    private Dependence dependence;


    public static LicenseTemplateResponse empty() {
        return LicenseTemplateResponse.builder()
                .responseStatus(null)
                .licenseTemplate(null)
                .dependence(null)
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(this.getLicenseTemplate());
    }
}
