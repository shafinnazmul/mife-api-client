package com.mashfin.licenseissuer.mife.api.endpoint.dto;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;


@Builder(builderClassName = "Builder", toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "errorResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = -3478889009099398271L;

    @XmlElement(name = "responseStatus")
    private ResponseStatus responseStatus;


    public static ErrorResponse empty() {
        return ErrorResponse.builder()
                .responseStatus(null)
                .build();
    }

    public boolean hasResult() {
        return Objects.nonNull(getResponseStatus());
    }
}
