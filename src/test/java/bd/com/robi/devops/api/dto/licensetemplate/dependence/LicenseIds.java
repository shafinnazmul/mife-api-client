package com.mashfin.licenseissuer.mife.api.endpoint.dto.licensetemplate.dependence;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class LicenseIds implements Serializable {

    private static final long serialVersionUID = -1585534642794554791L;

    private List<String> licenseId;

}
